(function($) {
  "use strict"; // Start of use strict

    $(document).on("click","#zip_file",function(e){
        e.preventDefault()
        var this_id = $(this).attr("data-attr")
        $.ajax({
            type: "POST",
            url: "swt-create",
            data: {"cid":this_id},
            success: function(msg) {
                // alert(msg)
                console.log(msg)
            }
        })
        return false
    })

  $(document).on("click","#generate_password",function(e){
    e.preventDefault()
      var result           = '';
      var result2 = '';
      var result3 = '';
      var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%&*{[]}<>?';
      var charactersLength = characters.length;
      for ( var i = 0; i < 18; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }

      $("#client_pword").val(result)

      return false
  })

  $(document).on("click","#add_user",function(){
        $("#user_name").val("");
        $("#user_email").val("");
        $("#user_pword").val("");
        $("#utoken").val("");
  })

  $(document).on("click","#save_user",function(e){
    e.preventDefault()

    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    var un = $("#user_name").val(),
        em = $("#user_email").val(),
        pw = $("#user_pword").val(),
        utoken = $("#utoken").val()

        if(em == "") {
          alert("Email is empty!")
          return false
        }else{
            if(!em.match(re)) {
                alert("Email address is not valid.")
                return false
            }
        }
        if(pw == "") {
          alert("Password is empty!")
          return false
        }

        if(em && pw != "") {
            $.ajax({
                type: "POST",
                url: "swt-saveUser",
                data: {"un":un, "em":em, "pw":pw ,"utoken": utoken}       
            }).done(function(success){
                if(success == 3) {
                    alert("Success! User successfully updated.")
                }else if(success == 2) {
                    alert("Warning! User already exist.")
                }else if(success == 1) {
                    alert("Sucess! User successfully added.")
                    setTimeout(function(){
                        location.reload()
                    },1000)
                }else{
                    alert("Error! Something went wrong please try again later.")
                }
            })
        }

    return false
  })

    $(document).on("click","#delete_user",function(e){
        let uid = $(this).attr("data-attr")
        $("#del_user").attr("data-attr",uid)
    })
    $(document).on("click","#del_user",function(e){
        e.preventDefault()
        var u_id = $(this).attr("data-attr")

        $.ajax({
            type: "POST",
            url: "swt-delUser",
            data: {"uid": u_id}
        }).done(function(success){
            if(success == 1) {
                alert("Success! User was successfully deleted.")
                setTimeout(function(){
                    location.reload()
                },1000)
            }else{
                alert("Warning! Something went wrong please try again later.")
            }
        })
        return false
    })

    $(document).on("click","#edit_user",function(e){
        var em = $(this).attr("data-attr")
        $.ajax({
            type: "POST",
            url :"swt-userInfo",
            data: {"email": em}
        }).done(function(success){
            var user = JSON.parse(success)

            $("#utoken").val(user[0].user_id)
            $("#user_name").val(user[0].name)
            $("#user_email").val(user[0].email)
            $("#user_pword").val(user[0].password)
        })
    })


  $(document).on('click',"#res_btn_ad",function(e){
    e.preventDefault()
    let email = $("#res_email").val(),
      result           = '',
      result2 = '',
      result3 = '',
      characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%*{[]}',
      charactersLength = characters.length

        for ( var i = 0; i < 18; i++ ) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

    $.ajax({
          type: "POST",
          url: "swt-respass",
          data: {"email": email, "pw": result},
          success: function(suc) {
            var msg_parse = JSON.parse(suc)

            if(msg_parse.success) {
              $("#alert_err").removeClass("alert-danger").addClass("in show alert-success").css({
                position: 'fixed',
                top: 50+"px"
              })
            }else{
              $("#alert_err").removeClass("in alert-success").addClass("alert-danger fade in show").css({
                position: 'fixed',
                top: 50+"px"
              })
            }

            $("#alert_bcntr").html(msg_parse.msg)
          }
    })
    return false
  })

  $(document).on("click","#add_client",function(e){
    $(".modal-title").text("Add Client")
    $("#client_name").val("")
    $("#client_email").val("")
    $("#client_pword").val("")
  })

  $(document).on("click","#save_client",function(e){
    e.preventDefault()
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    var c_name = $("#client_name").val(),
        c_email = $("#client_email").val(),
        c_pword = $("#client_pword").val(),
        c_premium = $("#premium_user:checked").val(),
        ctoken = $("#ctoken").val()

        if(c_email == "") {
            alert("Email is empty!")
            $("#client_email").focus()
            return false
        }else{
            if(!c_email.match(re)) {
                alert("Email is not valid!")
                return false
            }
        }

        if(c_pword == "") {
          alert("Password is empty!")
          $("#client_pword").focus()
          return false
        }

        if(c_email && c_pword) {

          $.ajax({
            type: "POST",
            url: "swt-cReg",
            data: {"c_name": c_name, "c_email": c_email, "c_pword":c_pword, "c_premium": c_premium, "ctoken": ctoken},
            success: function(data) {
              var msg_parse = JSON.parse(data)

              if(msg_parse.success) {
                alert(msg_parse.msg)
                location.reload()
              }else{
                alert(msg_parse.msg)
              }
              c_name = $("#client_name").val("")
              c_email = $("#client_email").val("")
              c_pword = $("#client_pword").val("")


            }
          })

        }
        

    return false
  })

  $(document).on("click","#edit_client",function(e){
    $(".modal-title").text("Edit Client")
    var client_id = $(this).attr("data-attr")
      $.ajax({
        type: "POST",
        url: "swt-clientInf",
        data: {"c_id": client_id},
        success: function(data) {
          var client_info  = JSON.parse(data)
          $("#ctoken").val(client_id)
          $("#client_name").val(client_info[0].name)
          $("#client_email").val(client_info[0].email)
          $("#client_pword").val(client_info[0].password)
          if(client_info[0].paid_premium == 1) {
            $("#premium_user").attr("checked","checked")
          }else{
            $("#premium_user").removeAttr("checked","checked")
          }
        }
      })
  })


  var cd_attr = ""

  $(document).on("click","#delete_client",function(e){
    e.preventDefault()

    cd_attr = $(this).attr("data-attr")

    return false;
  })

  $(document).on("click","#del_client",function(e){
    e.preventDefault()
    $.ajax({
        type: "POST",
        url: "swt-delClient",
        data: { "c_id": cd_attr},
        success: function(msg) {
          if(msg == 1) {
            alert("Client was successfully deleted!")
          }else{
            alert("An error has occured. Please try again.")
          }
          location.reload()
        }
      })
    return false
  })

    $(document).on("click", "#view_info",function(e){
        var cid = $(this).attr("data-attr")
        $("#cInfo_modal").find(".modal-body").html("<i class='fa fa-spinner spin'></i> Loading data....")
        $.ajax({
            type: "POST",
            url: "swt-viewData",
            data: {"cid": cid}        
        }).done(function(data){
            if(data == 0) {
                $("#cInfo_modal").find(".modal-body").html("<h3 class='text-warning'><i class='fa fa-exclamation-triangle'></i> No data was found!</h3>")  
            }else{
                $("#cInfo_modal").find(".modal-body").html(data)    
            }
        })
    })
  $(document).on("submit","#admin_reg_form",function(e){
    e.preventDefault()

    let admin_reg_info = $(this).serializeArray();

    $.ajax({
      type: "POST",
      url: "swt-reg",
      data: admin_reg_info,
      success: function(msg) {
        if(msg == 1) {
            alert("You have successfully registered! You can now login with your account!")
            setTimeout(function(e){
                location.reload()
            }, 300)
        }else if(msg == 2) {
            alert("Oops! password didn't match. Please double check it.")
            $("[type=password]").focus()
        }else{
            alert("Oops! email or password is/are incorrect please try again.")
        }
      }
    })

    return false
  })

  $(document).on("submit","#admin_log_form",function(e){
    e.preventDefault()

    let admin_log_info = $(this).serializeArray();

    $.ajax({
      type: "POST",
      url: "swt-adminLog",
      data: admin_log_info,
      success: function(msg) {
        if(msg == 1) {
          $("#log_admin").attr("disabled",'disabled').text("Loging you in...")
          setTimeout(function(e){
            location.reload()
          }, 300)
        }else{
          alert("Oops! email or password is/are incorrect please try again.")
        }
      }
    })

    return false
  })


  var editable_div = $(".input_area"),
      input_type = $(".input_type"),
      data_att = "",
      append_type = "",
      appended_input = "",
      input_req = "",
      req_txt = "",
      proceed = $(".proceed"),
      remove_data = $(".remove_data")

      $("div[contenteditable]").on("paste", function(e) {
         e.preventDefault();
         var text = e.originalEvent.clipboardData.getData("text/plain");
         document.execCommand("insertHTML", false, text);
      });

  var arr_inputs = [],
      // require_input = "",
      choice_yn_val = ""

  input_type.on("click",function(e){

    var result = '';
     var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
     var charactersLength = characters.length;
     for ( var i = 0; i < 4; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
     }

    data_att = $(this).data("attr")

    

    if(data_att == "title") {
      e.preventDefault()
      appended_input = '[h3 class="title"] <div contenteditable="true">Add title here.. </div>[/h3] <div class="opt_r"><a href="#" class="remove_data text-danger" title="remove"><i class="fa fa-times" contenteditable="false"></i></a> <i class="fa"></i></div>'
      $(this).parents(".parent_box").find(".input_area").append("<li class='input_item mb-3'>"+appended_input+"</li>")
    }else if(data_att == "question") {
      e.preventDefault()
      appended_input = appended_input = '[div class="div_question"]  <div contenteditable="true"> Add question here.. </div> [/div] <div class="opt_r"><a href="#" class="remove_data text-danger" title="remove" contenteditable="false"><i class="fa fa-times"></i></a> <i class="fa"></i></div>'
      $(this).parents(".parent_box").find(".input_area").append("<li class='input_item mb-3'>"+appended_input+"</li>")
    }else if(data_att == "text"){
      e.preventDefault()
      // if(confirm("Make field as required?")) {
      //   require_input = 'required="required"'
      // }else{
      //   require_input = ''
      // }
      appended_input = '[input type="text" class="form-control" value="" name="input_'+result+'"] <div class="opt_r"><a href="#" class="remove_data text-danger" title="remove" contenteditable="false"><i class="fa fa-times"></i></a> <i class="fa"></i></div>'
      $(this).parents(".parent_box").find(".input_area").append("<li class='input_item mb-3'  contenteditable='true'>"+appended_input+"</li>")
    }else if(data_att == "textarea"){
      e.preventDefault()
      // if(confirm("Make field as required?")) {
      //   require_input = 'required="required"'
      // }else{
      //   require_input = ''
      // }
      appended_input = '[textarea class="form-control" name="input_'+result+'"][/textarea] <div class="opt_r"><a href="#" class="remove_data text-danger" title="remove" contenteditable="false"><i class="fa fa-times"></i></a> <i class="fa"></i></div>'
      $(this).parents(".parent_box").find(".input_area").append("<li class='input_item mb-3'  contenteditable='true'>"+appended_input+"</li>")
    }else if(data_att == "email"){
      e.preventDefault()
      // if(confirm("Make field as required?")) {
      //   require_input = 'required="required"'
      // }else{
      //   require_input = ''
      // }
      appended_input = '[input type="email" class="form-control" value="" name="input_'+result+'"] <div class="opt_r"><a href="#" class="remove_data text-danger" title="remove" contenteditable="false"><i class="fa fa-times"></i></a> <i class="fa"></i></div>'
      $(this).parents(".parent_box").find(".input_area").append("<li class='input_item mb-3'  contenteditable='true'>"+appended_input+"</li>")
    }else if(data_att == "file"){
      e.preventDefault()
      // if(confirm("Make field as required?")) {
      //   require_input = 'required="required"'
      // }else{
      //   require_input = ''
      // }
      appended_input = '[input type="file" class="d_file" value="" name="filename[]" multiple="multiple"] <div class="opt_r"><a href="#" class="remove_data text-danger" title="remove" contenteditable="false"><i class="fa fa-times"></i></a> <i class="fa"></i></div>'
      $(this).parents(".parent_box").find(".input_area").append("<li class='input_item mb-3'  contenteditable='true'>"+appended_input+"</li>")
    }else if(data_att == "tel"){
      e.preventDefault()
      // if(confirm("Make field as required?")) {
      //   require_input = 'required="required"'
      // }else{
      //   require_input = ''
      // }
      appended_input = '[input type="tel" class="form-control" value="" name="input_'+result+'"] <div class="opt_r"><a href="#" class="remove_data text-danger" title="remove" contenteditable="false"><i class="fa fa-times"></i></a> <i class="fa"></i></div>'
      $(this).parents(".parent_box").find(".input_area").append("<li class='input_item mb-3'  contenteditable='true'>"+appended_input+"</li>")
    }else if(data_att == "select"){
      e.preventDefault()
      // if(confirm("Make field as required?")) {
      //   require_input = 'required="required"'
      // }else{
      //   require_input = ''
      // }
      appended_input = '[select class="form-control" name="input_'+result+'"]<div contenteditable="true" class="flex100">[option value="0"]First Option[/option]<br>[option value="1"]Second Option[/option]</div><br>[/select]<div class="opt_r"><a href="#" class="remove_data text-danger" title="remove" contenteditable="false"><i class="fa fa-times"></i></a> <i class="fa"></i></div>'
      $(this).parents(".parent_box").find(".input_area").append("<li class='input_item mb-3'>"+appended_input+"</li>")
    }else if(data_att == "checkbox"){
      e.preventDefault()
      // if(confirm("Make field as required?")) {
      //   require_input = 'required="required"'
      // }else{
      //   require_input = ''
      // }
      appended_input = '[input type="checkbox" name="input_'+result+'" value="yes"] Add text info here.. <div class="opt_r"><a href="#" class="remove_data text-danger" title="remove" contenteditable="false"><i class="fa fa-times"></i></a> <i class="fa"></i></div>'
      $(this).parents(".parent_box").find(".input_area").append("<li class='input_item mb-3' contenteditable='true'>"+appended_input+"</li>")
    }else if(data_att == "radio"){
      e.preventDefault()
      // if(confirm("Make field as required?")) {
      //   require_input = 'required="required"'
      // }else{
      //   require_input = ''
      // }
      appended_input = '[input type="radio" name="input_'+result+'" value="yes"] Add text info here.. <div class="opt_r"><a href="#" class="remove_data text-danger" title="remove" contenteditable="false"><i class="fa fa-times"></i></a> <i class="fa"></i></div>'
      $(this).parents(".parent_box").find(".input_area").append("<li class='input_item mb-3' contenteditable='true'>"+appended_input+"</li>")
    }

    $(".dropdown-menu").removeClass("show")

  })


  $(document).on("click",".remove_data",function(e){
    e.preventDefault()
    $(this).parents(".input_item").remove()
    return false
  })

  var data_info = "",
      cat = "",
      formid = "",
      test = ""

  $(document).on("click",".btn_submit",function(e){
    e.preventDefault()

    $(this).attr("disabled", "disabled").text("Saving..")

    cat = $(this).parents(".card-body").find(".category_id").val()
    formid = $(this).parents(".card-body").find(".form_id").val()

    test = $(this).parents(".card-body").find(".input_area")

    test.find("li").each(function(){
      arr_inputs.push($(this).text())
    })

    $.ajax({
      type: "POST",
      url: "swt-submit",
      data: {"data_info":arr_inputs, "cat":cat, "formid": formid},
      success: function(msg) {

        if(msg == 1) {
          alert("Data was successfully saved!")
        }else{
          alert("An error has occured. Please try again.")
        }

        $(".btn_submit").removeAttr("disabled", "disabled").text("Save")
        location.reload()
      }
    })


    return false
  })

    var flag_id = ""

    $(document).on("click","#edit_category",function(e){

      let cat_id = ""

      cat_id = $(this).attr("data-attr")

      $.ajax({
        type: "POST",
        url: "swt-categoryInfo",
        data: { "cat_id": cat_id},
        success: function(data) {
          let cat_parse = JSON.parse(data),
              cid = cat_parse[0].category_id,
              cn = cat_parse[0].category_name

              flag_id = cid

              $("#category_input").val(cn)
        }
      })

    })


  $(document).on("click","#save_category",function(e){
    e.preventDefault()

    var category = $("#category_input").val().toString().toLowerCase();

    $.ajax({
      type: "POST",
      url: "swt-saveCategory",
      data: { "cat": category, "catid": flag_id},
      success: function(msg) {
        if(msg == 1) {
          alert("Category was successfully added!")
        }else if(msg == 2){
          alert("Category was successfully updated!")
        }else{
          alert("An error has occured. Please try again.")
        }
        location.reload()
      }
    })

    return false
  })

  var c_attr = ""

  $(document).on("click","#delete_category",function(e){
    e.preventDefault()

    c_attr = $(this).attr("data-attr")

    return false;
  })

  $(document).on("click","#del_category",function(e){
      $.ajax({
        type: "POST",
        url: "swt-delCategory",
        data: { "c_id": c_attr},
        success: function(msg) {
          if(msg == 1) {
            alert("Category was successfully deleted!")
          }else{
            alert("An error has occured. Please try again.")
          }
          location.reload()
        }
      })
      
  })


  // Toggle the side navigation
  $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Close any open menu accordions when window is resized below 768px
  $(window).resize(function() {
    if ($(window).width() < 768) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
    if ($(window).width() > 768) {
      var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  });

  // Scroll to top button appear
  $(document).on('scroll', function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function(e) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    e.preventDefault();
  });

})(jQuery); // End of use strict
