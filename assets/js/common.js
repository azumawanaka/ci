$(function(){


	var total_success_form = 0
		
	loadInputs()
	
	function loadInputs() {
		var token_id = $("#token_session").val()
		$.ajax({
			type: "POST",
			url: "inputs",
			data: {"id":token_id},
			success: function(success) {
				var prs = JSON.parse(success),
				per_counter = 0,
				l_id = 0
				for(var i = 0; i < prs["inputs"].length; i++) {
					if( $("[name='"+prs["inputs"][i].data_key+"']").attr("type") == "radio" || 
						$("[name='"+prs["inputs"][i].data_key+"']").attr("type") == "checkbox" ) {
						if($("[name='"+prs["inputs"][i].data_key+"']").attr("class") == "app_book_choice") {
							console.log("Test: "+prs["inputs"][i].data_val)	
							if( prs["inputs"][i].data_val == "Use a Form" ) {
								$(".app_book_choice[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
								$(".app_book_choice[value='no']").removeAttr("checked","checked")
								$(".app_book_choice[value='Link to an external tool']").removeAttr("checked","checked")
							}else if( prs["inputs"][i].data_val == "Link to an external tool" ){
								$(".app_book_choice[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
								$(".app_book_choice[value='no']").removeAttr("checked","checked")
								$(".app_book_choice[value='Use a Form']").removeAttr("checked","checked")
							}else{
								$(".app_book_choice[value='no']").attr("checked","checked")
								$(".app_book_choice[value='Link to an external tool']").removeAttr("checked","checked")
								$(".app_book_choice[value='Use a Form']").removeAttr("checked","checked")
								$(".app_book_choice1").slideUp()
								$(".app_book_choice2").slideUp()
							}

						}else if($("[name='"+prs["inputs"][i].data_key+"']").attr("class") == "shop_choice") {
							if( prs["inputs"][i].data_val == "yes" ) {
								$(".shop_choice[name='"+prs["inputs"][i].data_key+"']").attr("checked","checked")
								$(".shop_yes").css({
									display: 'block'
								})
								$(".shop_no").css({
									display: 'none'
								})
							}else{
								$(".shop_yes").css({
									display: 'none'
								})
								$(".shop_no").css({
									display: 'block'
								})
								$(".shop_choice[name='"+prs["inputs"][i].data_key+"']").attr("checked","checked")
							}
						}else if($("[name='"+prs["inputs"][i].data_key+"']").attr("class") == "shop_choice_v2") {
							if( prs["inputs"][i].data_val == "yes" ) {
								$(".shop_choice_v2[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
								$(".shop_choice_v2[value='no']").removeAttr("checked","checked")
							}else{
								$(".shop_choice_v2[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
								$(".shop_choice_v2[value='yes']").removeAttr("checked","checked")
							}
						}else if($("[name='"+prs["inputs"][i].data_key+"']").attr("class") == "shop_choice_v3") {
							if( prs["inputs"][i].data_val == "yes" ) {
								$(".shop_choice_v3[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
								$(".shop_choice_v3[value='no']").removeAttr("checked","checked")
								$(".shop_show_yes3").css({
									display: 'block'
								})
								$(".shop_show_no3").css({
									display: 'none'
								})
							}else{
								$(".shop_choice_v3[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
								$(".shop_choice_v3[value='yes']").removeAttr("checked","checked")
								$(".shop_show_yes3").css({
									display: 'none'
								})
								$(".shop_show_no3").css({
									display: 'block'
								})
							}
						}else if($("[name='"+prs["inputs"][i].data_key+"']").attr("class") == "offers_choice") {
							if( prs["inputs"][i].data_val == "yes" ) {
								$(".offers_choice[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
								$(".offers_choice[value='no']").removeAttr("checked","checked")
								$(".offers_yes").css({
									display: 'block'
								})
								$(".offers_no").css({
									display: 'none'
								})
							}else{
								$(".offers_choice[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
								$(".offers_choice[value='yes']").removeAttr("checked","checked")
								$(".offers_yes").css({
									display: 'none'
								})
								$(".offers_no").css({
									display: 'block'
								})
							}
						}else if($("[name='"+prs["inputs"][i].data_key+"']").attr("class") == "img_choice") {
							if( prs["inputs"][i].data_val == "Single Image" ) {
								$(".img_choice[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
								$(".img_choice[value='Image Gallery']").removeAttr("checked","checked")
							}else{
								$(".img_choice[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
								$(".img_choice[value='Single Image']").removeAttr("checked","checked")
							}
						}else if($("[name='"+prs["inputs"][i].data_key+"']").attr("class") == "color_hexa") {
							if( prs["inputs"][i].data_val == "yes" ) {
								$(".color_hexa[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
								$(".color_hexa[value='no']").removeAttr("checked","checked")
							}else{
								$(".color_hexa[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
								$(".color_hexa[value='yes']").removeAttr("checked","checked")
							}

						}else if( $("[name='"+prs["inputs"][i].data_key+"']").attr("class") == "font_family") {

							if( prs["inputs"][i].data_val == "Open Sans" ) {
								$("[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
							}else if( prs["inputs"][i].data_val == "Source Sans Pro" ) {
								$("[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
							}else if( prs["inputs"][i].data_val == "Titillium Web" ) {
								$("[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
							}else if( prs["inputs"][i].data_val == "Arvo" ) {
								$("[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
							}else if( prs["inputs"][i].data_val == "Lora" ) {
								$("[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
							}else if( prs["inputs"][i].data_val == "Nunito" ) {
								$("[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
							}else if( prs["inputs"][i].data_val == "Raleway" ) {
								$("[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
							}else if( prs["inputs"][i].data_val == "Crimson Text" ) {
								$("[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
							}else{
								$("[value='"+prs["inputs"][i].data_val+"']").attr("checked","checked")
							}


						}else{

							if($("[name='"+prs["inputs"][i].data_key+"']").val() == prs["inputs"][i].data_val) {
								$("[name='"+prs["inputs"][i].data_key+"']").attr("checked","checked")
							}

						}
					}else if( prs["inputs"][i].data_key == "last_id" || prs["inputs"][i].data_key == "percentage" || prs["inputs"][i].data_key.indexOf("question_") > -1 || prs["inputs"][i].data_key.indexOf("title_txt_") > -1) {

					}else{

						if ( prs["inputs"][i].data_key.indexOf('file_txt_') > -1)
						{
							if(prs["inputs"][i].data_val !="") {
						  		$("[name='"+prs["inputs"][i].data_key+"']").parent(".form-group").append("Files Uploaded: "+prs["inputs"][i].data_val)
						  	}
						}

						$("[name='"+prs["inputs"][i].data_key+"']").val(prs["inputs"][i].data_val)
					}
				}

				l_id = $(".last_id").length

				if(l_id >= parseInt($("#tot_cat").val()) && prs["ids"].length == parseInt($("#tot_cat").val())) {
					$("#bma_div").html('<h3>Please note, once you agree to submit your responses they can not be changed.  If you are ready to have your app built, please click the button below.</h3><button type="button" class="btn btn-success btn-lg" id="build_app">Build my App</button>')
				}		


				for (var z = 0; z < prs["cats"].length; z++) {
					$("#category_"+prs['cats'][z].cid).find("[name='form_id']").val(prs['cats'][z].formID)
					$("#categ_"+prs['cats'][z].cid).val(prs['cats'][z].cid)
				}	


				for (var y = 0; y < prs["ids"].length; y++) {
					$("#lid_"+prs['ids'][y].cat).val(prs['ids'][y].ci_id)

						var prog_per = parseInt(prs["ids"][y].per),
							bg = "#e74a3b"

						if(prog_per >= 0 && prog_per <= 20) {
							bg = "#e74a3b"
						}else if(prog_per > 20 && prog_per <= 40) {
							bg = "#f6c23e"
						}else if(prog_per > 40 && prog_per <= 60) {
							bg = "#4e73df"
						}else if(prog_per > 60 && prog_per <= 80) {
							bg = "#36b9cc"
						}else{
							bg = "#1cc88a"
						}


						$("#prog_"+prs['ids'][y].cat).find("h4 span").html(prog_per+"%")

						$("#percent_"+prs['ids'][y].cat).val(prog_per)

						$("#prog_"+prs['ids'][y].cat+" .progress-bar").css({
							backgroundColor: bg
						})

						$("#prog_"+prs['ids'][y].cat).find(".progress-bar").attr("aria-valuenow",prog_per)
						$("#prog_"+prs['ids'][y].cat).find(".progress-bar").attr("aria-valuemax",prog_per).css({
							width: prog_per+"%"
						})
							
				}
			}
		})
	}

	$(document).on("click", ".app_book_choice",function(e){

		var selected = $("input[type='radio'][name='input_nvaU']:checked"),
			selectedVal = ""

		if (selected.length > 0) {
		    selectedVal = selected.attr("id")
		    if(selectedVal == "c1") {
				$(".app_book_choice1, .app_book_choice2").slideUp()
			}else if(selectedVal == "c2"){
				$(".app_book_choice1").slideDown()
				$(".app_book_choice2").slideUp()
			}else{
				$(".app_book_choice1").slideUp()
				$(".app_book_choice2").slideDown()
			}
		}

	})

	$(document).on("click", ".shop_choice",function(e){

		var selected2 = $("input[type='radio'][name='input_Znkn']:checked"),
			selectedVal2 = ""

		if (selected2.length > 0) {
		    selectedVal2 = selected2.val()
		    if(selectedVal2 == "yes") {
				$(".shop_yes").fadeIn()
				$(".shop_no").fadeOut()
			}else{
				$(".shop_yes").fadeOut()
				$(".shop_no").fadeIn()
			}
		}

	})

	$(document).on("click", ".offers_choice",function(e){

		var selected2 = $("input[type='radio'][name='input_Znlp']:checked"),
			selectedVal2 = ""

		if (selected2.length > 0) {
		    selectedVal2 = selected2.val()
		    if(selectedVal2 == "yes") {
				$(".offers_yes").fadeIn()
				$(".offers_no").fadeOut()
			}else{
				$(".offers_yes").fadeOut()
				$(".offers_no").fadeIn()
			}
		}

	})

	var flag = 0
	$(document).on("click",".color_hexa",function(e){
		var radioValue = $(".color_hexa:checked").val(),
			characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%*{[]}',
			charactersLength = characters.length,
			result = ""

	    for ( var i = 0; i < 4; i++ ) {
	        result += characters.charAt(Math.floor(Math.random() * charactersLength));
	    }

        if(radioValue == "yes"){
        	flag = 1
           	$(".color_div").html("<textarea class='form-control nom_col' name='nominated_color_hex' placeholder='Add Your Colors Here..'></textarea>")
  
           	$(this).parents(".save_inputs").find(".per_token").val( parseInt($(this).parents(".save_inputs").find(".per_token").val())+1 )
           	// $(".color_show").css({
           	// 	display: "block!important"
           	// });

        }else{
        	if(flag == 1) {
        		$(this).parents(".save_inputs").find(".per_token").val( parseInt($(this).parents(".save_inputs").find(".per_token").val())-1 )
        	}
        	// $(".color_div .nom_col").html("").remove()
        	// $(".color_show").css({
         //   		display: "none!important"
         //   	});
        }
	})

	$("#pg-client").find(".tab-pane").each(function(){ 
		var this_tab = $(this),
			count_notempty = 0
			this_tab.find(".form-group").each(function(){

				if($(this).find("input[type=text], input[type=file], input[type=url], textarea").val() == '') {
					count_notempty++
				}

				this_tab.find(".per_token").val(count_notempty)

			})
	})

	$(document).on("change keyup","input[type=text], input[type=file], input[type=url], input[type=checkbox], textarea",function(e){
		var count_empty = 0,
			tot_count = $(this).parents(".save_inputs").find(".total_count").val(),
			perc = 0,
			prog_per = 0,
			bg = "",
			count_notempty_input = 0,
			per_token = $(this).parents(".save_inputs").find(".per_token").val()

		$(this).parents(".input_area").find(".form-group").each(function(){ 
			if($(this).find("input[type=text], input[type=url], input[type=file], textarea").val() == '') {
				count_notempty_input++
			}
		})

		perc = parseFloat(count_notempty_input/per_token * 100)

		prog_per = 100 - Math.round(perc)

		if(prog_per > 0 && prog_per <= 20) {
			bg = "#e74a3b"
		}else if(prog_per > 20 && prog_per <= 40) {
			bg = "#f6c23e"
		}else if(prog_per > 40 && prog_per <= 60) {
			bg = "#4e73df"
		}else if(prog_per > 60 && prog_per <= 80) {
			bg = "#36b9cc"
		}else{
			bg = "#1cc88a"
		}

		$(this).parents(".save_inputs").find(".progress-bar").css({
			backgroundColor: bg
		})

		$(this).parents(".save_inputs").find(".prog h4 span").text(prog_per+"%")
		$(this).parents(".save_inputs").find(".progress-bar").attr("aria-valuenow",prog_per)
		$(this).parents(".save_inputs").find(".progress-bar").attr("aria-valuemax",prog_per).css({
			width: prog_per+"%"
		})

		$(this).parents(".save_inputs").find(".percentage").val(prog_per)
	})

	$(document).on('click',"#res_btn",function(e){
		e.preventDefault()
		let email = $("#res_email_client").val(),
			result           = '',
			result2 = '',
			result3 = '',
			characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%*{[]}',
			charactersLength = characters.length

	      for ( var i = 0; i < 18; i++ ) {
	        result += characters.charAt(Math.floor(Math.random() * charactersLength));
	      }

		$.ajax({
			type: "POST",
			url: "respass",
			data: {"email": email, "pw": result},
			success: function(suc) {
				var msg_parse = JSON.parse(suc)

				if(msg_parse.success) {
					$("#alert_err").removeClass("alert-danger").addClass("in show alert-success").css({
						position: 'fixed'
					})

				}else{
					$("#alert_err").removeClass("in alert-success").addClass("alert-danger fade in show").css({
						position: 'fixed'
					})
				}

				$("#alert_bcntr").html(msg_parse.msg)
				
			}
		})
		return false
	})

	$(document).on("click","#forgot .close",function(e){
		$("#alert_err").css({
			position: 'absolute'
		})
	})

	$(document).on("change",".d_file",function(e){
		var files = $(this)[0].files
		var file_arr = []
		for(var count = 0; count<files.length; count++) {
		   var name = files[count].name
		   file_arr.push(name)
		}
		$(this).parent(".form-group").find(".file_txt").val(file_arr)
	})

	$(document).on("submit",".save_inputs",function(e){
		e.preventDefault()

		var this_form = $(this)

		this_form.find(".submit-form").attr("disabled","disabled").text("Saving..")

		$.ajax({
			type: 'POST',
			url: 'save_form',
			data: new FormData(this),
			processData:false,
			contentType:false,
			cache:false,
			async:false,
			success: function(msg) {
				var msg_parse = JSON.parse(msg),
				lastid = msg_parse.last_id

				if(msg_parse.success) {

					total_success_form++

					$("#alert_err").removeClass("alert-danger").addClass("in alert-success")
					this_form.find(".submit-form").removeAttr("disabled","disabled").text("Update Inputs")
				}else{
					$("#alert_err").removeClass("in alert-success").addClass("alert-danger")
					this_form.find(".submit-form").removeAttr("disabled","disabled").text("Save Inputs")
				}

				$("#alert_bcntr").html(msg_parse.messages)
				this_form.find(".last_id").val(lastid)

				setTimeout(function(){
					$("#alert_err").removeClass("in alert-danger")
				}, 2500)

				if(total_success_form >= parseInt($("#tot_cat").val())) {
					$("#bma_div").html('<h3>Please note, once you agree to submit your responses they can not be changed.  If you are ready to have your app built, please click the button below.</h3><button type="button" class="btn btn-success btn-lg" id="build_app">Build my App</button>')
				}

			}
		})
		
		return false
	})

	$(document).on("click","#build_app",function(e){
		e.preventDefault()

		var sess_id = $("#token_session").val()
		// send info on admin through email
		$.ajax({
			type: "POST",
			url: "swt-notifyAdmin",
			data: {"sess_id":sess_id}
		}).done(function(msg) {
			if(msg == 1) {
				$("#alert_bcntr").html('<strong>Success!</strong> You have successfully save data.')
				$("#alert_err").removeClass("alert-danger").addClass("in alert-success")
				setTimeout(function(){
					location.reload()
				}, 2000)
			}else{
				$("#alert_bcntr").html('<strong>Warning!</strong> Something went wrong. Please try again later.')
				$("#alert_err").removeClass("alert-success").addClass("in alert-danger")
				setTimeout(function(){
					$("#alert_bcntr").html("")
					$("#alert_err").removeClass("in alert-danger")
				}, 2000)
			}
		})
		
		return false
	})


	$(document).on("submit","#user_log_form",function(e){
		e.preventDefault()

		var email = $("#email").val(),
			password = $("#password").val()
			$("#log_user").attr("disabled", "disabled").text("logging in..")

			$.ajax({
				type: "POST",
				url: "swt-uSign",
				data: {"email":email, "pword": password},
				success: function(data) {
					if(data == 1) {
						location.reload()
					}else{
						alert("Oops! Email or password is/are incorrect!")
					}
					$("#email").val("")
					$("#password").val("")
					$("#log_user").removeAttr("disabled", "disabled").text("Login")
				}
			})

		return false
	})

	$(document).on("click","#logout",function(e){
		e.preventDefault()

		// check if forms already submitted
		$("body").find("#log_err").remove()

		var fc = $('.fc'),
			fc_id = "",
			fc_counter = 0,
			fc_val = ""

		fc.each(function(e){
			fc_id = $(this).attr("data-attr")
			if($("#lid_"+fc_id).val() == "" && $("#percent_"+fc_id).val() > 0) {
				fc_counter++
				fc_val += '<li>'+$("#fcid_"+fc_id).val()+'</li>'		
			}
		})
		if(fc_counter > 0) {
			$("body").prepend('<div style="margin-top: 80px" id="log_err" class="alert alert-warning alert-dismissible fade show in container" role="alert">'
						  +'<strong>Oops!</strong> Please save your changes inside <ol style="padding-left: 20px;margin-top: 10px;">'+fc_val
						  +'</ol></div>')
			window.scrollTo(0, 0)
			setTimeout(function(){
				$("body").find("#log_err").remove()
			}, 5000)
		}else{
			$(this).text('Logging out..')
			$.ajax({
				type : "POST",
				url : "logout",
				success: function(data) {
					if(data) {
						location.reload()
					}
				}

			})
		}


		return false
	})

})