<?php

class DashboardModel extends CI_Model {


    public function saveAdminInfo($data) {
        return $this->db->insert('user_tbl',$data);
    }

    public function getAdminCount() {
        $query = $this->db->get('user_tbl');
        $result = $query->num_rows();

        return $result;
    }

    public function getUserAdmin() {
        $this->db->where("access_mode !=", 1);
        $query = $this->db->get("user_tbl");
        $result = $query->result();

        return $result;
    }

    public function updateAdminInfo($data, $token) {
        $this->db->where("user_id",$token);
        return $this->db->update('user_tbl', $data);
    }

    public function userDelete($uid) {
        return $this->db->delete("user_tbl", array("user_id" => $uid));
    }

    public function checkAdmin($email) {
        $this->db->where("email",$email);
        $query = $this->db->get('user_tbl');
        $result = $query->result();

        return $result;
    }

    public function checkAdminID($user_id) {
        $this->db->where("user_id",$user_id);
        $query = $this->db->get('user_tbl');
        $result = $query->result();

        return $result;
    }

    public function updatePassword($new_password,$email) {
        $this->db->where("email", $email);
        return $this->db->update('user_tbl', array("password"=>$new_password));
    }

    public function updateLastAccess($email) {
        $this->db->where("email",$email);
        return $this->db->update('user_tbl',array("last_access" => strtotime("now")));
    }

    public function saveFormNow($data)
    {
        return $this->db->insert('form_tbl',$data);
    }

    public function updateForm($data, $form_id)
    {
        $this->db->where("form_id",$form_id);
        return $this->db->update('form_tbl',$data);
    }

    public function getForm() {

        $query = $this->db->get('form_tbl');
        $result = $query->result();

        return $result;
    }

    public function saveCategory($data) {
        return $this->db->insert('form_category',$data);
    }

    public function updateCategory($data, $catid) {
        $this->db->where("category_id",$catid);
        return $this->db->update('form_category',$data);
    }

    public function getCategoryInfo($cat_id) {

        $this->db->where("category_id",$cat_id);
        $query = $this->db->get('form_category');
        $result = $query->result();

        return $result;
    }

    public function getFormCategory() {
        $query = $this->db->get('form_category');
        $result = $query->result();

        return $result;
    }

    public function delCategory($c_id) {
        // delete form inside 
        $this->db->delete("form_tbl", array("category_id" => $c_id));
        return $this->db->delete('form_category', array('category_id' => $c_id));
    }

    public function getClients() {
        $query = $this->db->get('client_tbl');
        $result = $query->result();

        return $result;
    }

    public function getQA() {
        $query = $this->db->get('form_tbl');
        $result = $query->result();

        return $result;
    }

    public function getCdata() {
        $query = $this->db->get('client_inputs');
        $result = $query->result();

        return $result;
    }

    public function saveClient($data) {
        return $this->db->insert('client_tbl',$data);
    }

    public function showClientInfo($cid) {
        $this->db->where("client_id", $cid);
        $query = $this->db->get('client_tbl');
        $result = $query->result();

        return $result;
    }

    public function checkClient($email) {
        $this->db->where("user_email", $email);
        $query = $this->db->get('client_tbl');
        $result = $query->num_rows();

        return $result;
    }


    public function delClient($cid) {
        return $this->db->delete('client_tbl', array('client_id' => $cid));
    }

    public function updateClient($data, $ctoken) {
        $this->db->where("client_id",$ctoken);
        return $this->db->update('client_tbl',$data);
    }

    public function getClientData($cid) {
        $this->db->where("client_id", $cid);
        $this->db->order_by("category_id", "ASC");
        $query = $this->db->get('client_inputs');
        $result = $query->result();

        return $result;
    }

    public function getFormCategoryByID($value) {
        $this->db->where("category_id", $value);
        $query = $this->db->get('form_category');
        $result = $query->result();

        return $result;
    }

    public function getClientInfos($user_id) {
        $this->db->where("client_id", $user_id);
        $query = $this->db->get("client_tbl");
        $result = $query->result();

        return $result;
    }

    public function getClientID($email) {
        $this->db->where("user_email", $email);
        $query = $this->db->get("client_tbl");
        $result = $query->result();

        return $result;
    }

}

?>