<?php

class ClientModel extends CI_Model {

	public function checkClient($email) {
        $this->db->where("user_email", $email);
        $query = $this->db->get('client_tbl');
        $result = $query->result();

        return $result;
    }

    public function updateLastAccess($email) {
		$this->db->where("user_email",$email);
        return $this->db->update('client_tbl',array("last_access" => strtotime("now")));
	}

    public function saveInputs($data_info) {
        $this->db->insert('client_inputs',$data_info);
        return $this->db->insert_id();
    }

    public function updateInputs($data_info, $lastid) {
        $this->db->where("ci_id",$lastid);
        $this->db->update('client_inputs', $data_info);
        return $lastid;
    }

    public function updatePassword($new_password,$email) {
        $this->db->where("user_email",$email);
        return $this->db->update('client_tbl', array("password"=>$new_password));
    }

    public function viewInputs($cid) {
        $this->db->select('ci_data');
        $this->db->where("client_id", $cid);
        $query = $this->db->get('client_inputs');
        $result = $query->result();

        return $result;
    }

    public function checkFs($cid) {
        $this->db->select("final_submit");
        $this->db->where("client_id", $cid);
        $query = $this->db->get("client_tbl");
        $result = $query->result();

        return $result;
    }

    public function updateClient($user_id) {
        $this->db->where("client_id",$user_id);
        return $this->db->update('client_tbl', array("final_submit"=>1));
    }

    public function getFormID($cat_id) {
        $this->db->select('form_id');
        $this->db->where("category_id", $cat_id);
        $query = $this->db->get('form_tbl');
        $result = $query->result();

        return $result;
    } 
}