  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center" id="forgot">

      <div class="col-xl-6 col-lg-6 col-md-9 col-xs-12">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-xs-12">
                  <div class="alert alert-danger alert-dismissible fade" role="alert" id="alert_err" style="position: absolute;">
                    <div id="alert_bcntr" style="display: inline-block;"></div>
                    <!-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button> -->
                  </div>
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-2">Forgot Your Password?</h1>
                    <p class="mb-4">We get it, stuff happens. Just enter your email address below and we'll send you a link to reset your password!</p>
                  </div>
                  <form class="user">
                    <div class="form-group">
                      <input type="email" name="email" class="form-control form-control-user" id="res_email" aria-describedby="emailHelp" placeholder="Enter Email Address...">
                    </div>
                    <button type="button" class="btn btn-primary btn-user btn-block" id="res_btn_ad">
                      Reset Password
                    </button>
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small" href="swt-admin">Already have an account? Login!</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>