<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Form Category</h1>
  </div>

  <!-- Content Row -->
  <div class="row">
    <div class="card shadow mb-4 is-wide">
      <div class="card-header py-3">
        <a href="#" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#category_modal"><i class="fa fa-plus"></i> Add Category </a>
      </div>
      <div class="card-body">
       <div class="table-responsive">
          <table class="table table-bordered" id="category_tbl" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>ID</th>
                <th>Category Name</th>
                <!-- <th>Status</th> -->
                <th>Options</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($formCategory as $cat): ?>
                <tr>
                  <td>
                    <?php echo $cat->category_id; ?>
                  </td>
                  <td>
                    <?php echo $cat->category_name; ?>
                  </td>
                  <!-- <td> -->
                    <?php 
                    // if($cat->status == 1) {
                    //   echo "<strong class='text-info'>Available</strong>";
                    // }else{
                    //   echo "<strong class='text-warning'>Not Available</strong>";
                    // } 
                    ?>
                  <!-- </td> -->
                  <td>
                    <a href="#" id="edit_category" data-attr="<?php echo $cat->category_id; ?>" class="text-info" data-toggle="modal" data-target="#category_modal">
                      <i class="fa fa-edit"></i>edit
                    </a>
                    |
                    <a href="#" id="delete_category" data-attr="<?php echo $cat->category_id; ?>" class="text-danger" data-toggle="modal" data-target="#category_modal2">
                      <i class="fa fa-trash"></i>delete
                    </a>
                  </td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
  </div>

</div>

<!-- Modal -->
<div id="category_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Category</h4>
      </div>
      <div class="modal-body">
        <label>Category Name: </label>
        <input type="text" class="form-control" required="required" placeholder="" id="category_input">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="save_category" data-dismiss="modal">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="category_modal2" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h4 class="cont">Continue delete this category?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="del_category" data-dismiss="modal">Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>