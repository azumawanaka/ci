<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Users</h1>
  </div>

  <!-- Content Row -->
  <div class="row">
    <div class="card shadow mb-4 is-wide">
      <div class="card-header py-3">
        <a href="#" class="btn btn-secondary btn-sm" id="add_user" data-toggle="modal" data-target="#user_modal"><i class="fa fa-plus"></i> Add User </a>
      </div>
      <div class="card-body">
       <div class="table-responsive">
          <table class="table table-bordered" id="category_tbl" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>USER ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Date Registered</th>
                <th>Last Access</th>
                <th>Options</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($users as $u): ?>
                <tr>
                  <td>
                    <?php echo $u->user_id; ?>
                  </td>
                  <td>
                    <?php echo $u->name; ?>
                  </td>
                  <td>
                    <?php echo $u->email; ?>
                  </td>
                  <td>
                    <?php echo date("d M, Y", $u->date_registered); ?>
                  </td>
                  <td>
                    <?php
                      if(!empty($u->last_access)) {
                        echo date("d M, Y H:i", $u->last_access);
                      }
                    ?>
                  </td>
                  <td>
                    <a href="#" id="edit_user" data-attr="<?php echo $u->email; ?>" class="text-gray-800" data-toggle="modal" data-target="#user_modal">
                      <i class="fa fa-edit"></i>edit
                    </a>
                    |
                    <a href="#" id="delete_user" data-attr="<?php echo $u->user_id; ?>" class="text-danger" data-toggle="modal" data-target="#del_modal">
                      <i class="fa fa-trash"></i>delete
                    </a>
                  </td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
  </div>

</div>
<div id="user_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="utoken" id="utoken">
        <label>Name <span class="text-danger">*</span>: </label>
        <input type="text" class="form-control mb-3" required="required" name="user_name" placeholder="" id="user_name">
        <label>Email Address <span class="text-danger">*</span>: </label>
        <input type="email" class="form-control" required="required" name="user_email" placeholder="" id="user_email">
        <p><small class="text-info"><i class="fa fa-warn">!</i>This is will be used as his/her username</small></p>
        <div id="password-div">
          <label>Password <span class="text-danger">*</span>: </label>
          <input type="text" class="form-control mb-2" required="required" placeholder="" name="user_pword" value="" id="user_pword">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="save_user">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<div id="del_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <h4 class="cont">Continue delete this user?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="del_user">Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>