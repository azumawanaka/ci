<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Clients</h1>
  </div>

  <!-- Content Row -->
  <div class="row">
    <div class="card shadow mb-4 is-wide">
      <div class="card-header py-3">
        <a href="#" class="btn btn-secondary btn-sm" id="add_client" data-toggle="modal" data-target="#client_modal"><i class="fa fa-plus"></i> Add Client </a>
      </div>
      <div class="card-body">
       <div class="table-responsive">
          <table class="table table-bordered" id="category_tbl" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Date Registered</th>
                <th>Last Access</th>
                <th>Options</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($clients as $cli): ?>
                <tr>
                  <td>
                    <?php echo $cli->client_id; ?>
                  </td>
                  <td>
                    <?php echo $cli->name; ?>
                  </td>
                  <td>
                    <?php echo $cli->user_email; ?>
                  </td>
                  <td>
                    <?php echo date("d M, Y", $cli->date_registered); ?>
                  </td>
                  <td>
                    <?php
                      if(!empty($cli->last_access)) {
                        echo date("d M, Y H:i", $cli->last_access);
                      }
                    ?>
                  </td>
                  <td>
                    <a href="#" id="view_info" data-attr="<?php echo $cli->client_id; ?>" class="text-info" data-toggle="modal" data-target="#cInfo_modal">
                      <i class="fa fa-eye"></i> data
                    </a>
                    |
                    <a href="#" id="edit_client" data-attr="<?php echo $cli->client_id; ?>" class="text-gray-800" data-toggle="modal" data-target="#client_modal">
                      <i class="fa fa-edit"></i>edit
                    </a>
                    |
                    <a href="#" id="delete_client" data-attr="<?php echo $cli->client_id; ?>" class="text-danger" data-toggle="modal" data-target="#del_modal">
                      <i class="fa fa-trash"></i>delete
                    </a>
                   <!--  |
                    <a href="#" id="zip_file" data-attr="<?php echo $cli->client_id; ?>" class="text-warning">
                      <i class="fa fa-file-archive"></i> download
                    </a> -->
                  </td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
  </div>

</div>
<div id="client_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Client</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="client_id" id="ctoken">
        <label>Client Name <span class="text-danger">*</span>: </label>
        <input type="text" class="form-control mb-3" required="required" name="client_name" placeholder="" id="client_name">
        <label>Email Address <span class="text-danger">*</span>: </label>
        <input type="email" class="form-control" required="required" name="client_email" placeholder="" id="client_email">
        <p><small class="text-info"><i class="fa fa-warn">!</i>This is will be used as his/her username</small></p>
        <div id="password-div">
          <label>Password <span class="text-danger">*</span>: </label>
          <input type="text" class="form-control mb-2" required="required" placeholder="" name="client_pword" value="" id="client_pword">
          <!-- <a href="#" class="btn btn-xs btn-secondary" id="generate_password">Generate Password</a> -->
        </div>
        <label><input type="checkbox" name="premium_user" id="premium_user" value="1"> Paid Premium</label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="save_client">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<div id="del_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <h4 class="cont">Continue delete this client?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="del_client">Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<div id="cInfo_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header dblock">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Client's Data</h4>
      </div>
      <div class="modal-body">
        
      </div>
    </div>
  </div>
</div>