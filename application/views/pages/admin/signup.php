<div class="container">

  <!-- Outer Row -->
  <div class="row justify-content-center">

    <div class="col-xl-6 col-lg-6 col-md-6 col-xs-12">

      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-12">
              <div class="p-5">
                <div class="text-center">
                  <h1 class="h4 text-gray-900 mb-4">SWT - ADMIN</h1>
                </div>
                <form class="user" id="admin_reg_form">
                  <div class="form-group">
                    <label for="password"> Full Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-user" name="name" minlength="2" placeholder="John Jofard Gosh" required="required">
                  </div>
                  <div class="form-group">
                    <label for="password"> Email Address <span class="text-danger">*</span></label>
                    <input type="email" class="form-control form-control-user" name="email" aria-describedby="emailHelp" placeholder="example@gmail.com" required="required">
                  </div>
                  <div class="form-group">
                    <label for="password"> Password <span class="text-danger">*</span></label>
                    <input type="password" class="form-control form-control-user" name="password" placeholder="********" minlength="4" maxlength="50" required="required">
                  </div>
                  <div class="form-group">
                    <label for="password"> Re-enter Password <span class="text-danger">*</span></label>
                    <input type="password" class="form-control form-control-user" name="passwordv2" placeholder="********" minlength="4" maxlength="50" required="required">
                  </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block" id="reg_admin">Register</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>

</div>