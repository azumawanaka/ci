
<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Form</h1>
  </div>

  <!-- Content Row -->
  <div class="row">

      <div class="col-xs-12 is-wide mb-4">

        <nav>
          <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
            <?php $active_tab = ""; $counter = 0; foreach ($formCategory as $cat): $counter++;
                $new_cat = str_replace(" ","-",$cat->category_name);
                if($counter == 1) {
                  $active_tab = "active";
                }else{
                  $active_tab = "";
                }
              ?>
              <a class="nav-item nav-link  <?php echo $active_tab; ?>" id="nav-<?php echo $new_cat; ?>-tab" data-toggle="tab" href="#nav-<?php echo $new_cat; ?>" role="tab" aria-controls="nav-<?php echo $new_cat; ?>" aria-selected="true"><?php echo $cat->category_name; ?></a>
            <?php endforeach; ?>
          </div>
        </nav>

        <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">

          <?php  $counters = 0; foreach ($formCategory as $cat): $counters++;
                $new_cat = str_replace(" ","-",$cat->category_name);
                $cat_id = $cat->category_id;

                if($counters == 1) {
                  $active_tab = "active";
                }else{
                  $active_tab = "";
                }
              ?>
              <div class="tab-pane fade show <?php echo $active_tab; ?>" id="nav-<?php echo $new_cat; ?>" role="tabpanel" aria-labelledby="nav-<?php echo $cat->category_name; ?>-tab">
           
                <div class="card shadow parent_box">
                  <div class="card-header py-3 d-flex flex-row align-items-center form_headers">

                    <div class="form_headers-item">
                      <a class="btn btn-warning btn-sq input_type" data-attr="title" href="#">
                        <i class="fa fa-plus"></i> Add Title
                      </a>
                    </div>
                    <div class="form_headers-item">
                      <div class="form_headers-item dropdown no-arrow is-wide_sp">
                        <a class="dropdown-toggle btn btn-primary btn-sq" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Insert Input Type
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
                          <div class="dropdown-header">Choose Input Types:</div>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item input_type" href="#" data-attr="text">Text</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item input_type" data-attr="textarea" href="#">Text Area</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item input_type" href="#" data-attr="email">Email</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item input_type" href="#" data-attr="url">Url</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item input_type" href="#" data-attr="file">File</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item input_type" href="#" data-attr="tel">Tel</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item input_type" data-attr="select" href="#">Select</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item input_type" href="#" data-attr="checkbox">Checkbox</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item input_type" href="#" data-attr="radio">Radio</a>
                          </div>
                      </div>
                    </div>
                    <div class="form_headers-item">
                      <a class="form_headers-item btn btn-success btn-sq input_type" href="#" data-attr="question">
                        <i class="fa fa-plus"></i> Add Question
                      </a>
                    </div>

                  </div>
                  <div class="card-body">
                    <!-- <div class="text-right mb-3"><a href="#" class="btn btn-xs btn-info"><i class="fa fa-sort"></i> Activate Sorting</a></div> -->
                    <div class="form_inputs">
                      <input type="hidden" name="category_id" value="<?php echo $cat_id; ?>" class="category_id">

                      <?php foreach ($formdata as $styles) { 
                              if( $styles->category_id == $cat_id ) { ?>
                                <input type="hidden" name="form_id" value="<?php echo $styles->form_id; ?>" class="form_id">
                          <?php } 
                        } ?>
                      <ul class="input_area">
                        <?php foreach ($formdata as $styles) {
                            if( $styles->category_id == $cat_id ) {

                              $content  = json_decode($styles->input_arr);
                              $word_text = '[input';
                              $word_textarea = '[textarea';

                              foreach ($content as $cont) {

                                if(strpos($cont, $word_text) !== false || strpos($cont, $word_textarea) !== false){

                                  echo "<li class='input_item mb-3'><div contenteditable='true'>".$cont.'</div><div class="opt_r"><a href="#" class="remove_data text-danger" title="remove"><i class="fa fa-times" contenteditable="false"></i></a></div></li>';
                                }else{

                                  echo "<li class='input_item mb-3'><div contenteditable='true'>".$cont.'</div><div class="opt_r"><a href="#" class="remove_data text-danger" title="remove"><i class="fa fa-times" contenteditable="false"></i></a></div></li>';
                                }
                              }
                              
                             
                            }
                        } ?>
                      </ul>
                      
                    </div>
                    <button type="button" class="btn btn-info btn-sm btn_submit">Save</button>
                  </div>
                </div>
              </div>

          <?php endforeach; ?>

        </div>
      </div>
  </div>

</div>

