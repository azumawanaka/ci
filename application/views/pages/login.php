<div class="container">

  <!-- Outer Row -->
  <div class="row justify-content-center">

    <div class="col-lg-4 col-md-6 col-xs-12">

      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-12">
              <div class="p-4">
                <div class="text-center">
                  <h1 class="h4 text-gray-900 mb-4">Signin</h1>
                </div>
                <form class="user" id="user_log_form">
                  <div class="form-group">
                    <label for="password"> Email Address</label>
                    <input type="email" class="form-control form-control-user" id="email" name="email" aria-describedby="emailHelp" placeholder="example@gmail.com" required="required">
                  </div>
                  <div class="form-group">
                    <label for="password"> Password</label>
                    <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="********" minlength="4" maxlength="50" required="required">
                  </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block" id="log_user">Login</button>
                </form>
               <hr>
                <div class="text-center">
                  <a class="small" href="forgotpassword">Forgot Password?</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>

</div>