
<?php 
	$is_paid = $_SESSION['paid_premium'];
	$final_submit = $fs[0]->final_submit;
?>


<input type="hidden" id="token_session" value="<?=$_SESSION['cid'];?>">
<div class="container">
		<div class="intro">
			<p>
				Please complete the following questions.  <br>
				You can move back and forth through them, so you can always return to those questions you can not answer immediately.<br>  If you do not want a field to be included, please leave it blank.
			</p>
			<p>You can save and return to your responses at any time.</p>
			<p>Please note, the building of the app will not commence until you have finished your submission.</p>
		</div>
		<!-- Main component for a primary marketing message or call to action -->
		<div class="jumbotron">
			

			<?php if($final_submit == 1) : ?>

			    <?php 
					echo $htm;
				?>
			<?php else: ?>

				<ul class="nav nav-tabs">
				   <?php $active_tab = ""; $counter = 0; foreach ($formCategory as $cat):
					$new_cat = str_replace(" ","-",$cat->category_name);
					
					$dnone = "";

					if($is_paid == 0 && $new_cat == "extra-features") {}else{  $counter++;
						if($counter == 1) {
						  $active_tab = "active";
						}else{
						  $active_tab = "";
						}
					
				  ?>
				  <input type="hidden" class="fc" id="fcid_<?php echo $cat->category_id; ?>" data-attr="<?php echo $cat->category_id; ?>" value="<?php echo strtoupper($cat->category_name); ?>">
				  <li class="<?php echo $active_tab; ?>"><a data-toggle="tab" href="#<?php echo $new_cat; ?>"><?php echo strtoupper($cat->category_name); ?></a></li>
				<?php } 
				endforeach; ?>
				</ul>
				<input type="hidden" id="tot_cat" value="<?=$counter?>">
				<div class="tab-content">
					<?php $active_tab = ""; $counter = 0;  $counter3 = 0;
					foreach ($formCategory as $cat): 
						$new_cat = str_replace(" ","-",$cat->category_name);
						$new_cat_id = $cat->category_id;

						if($is_paid == 0 && $new_cat == "extra-features") {}else{  $counter++;
							$counter3++;
							
							if($counter == 1) {
							  $active_tab = "in active";
							}else{
							  $active_tab = "";
							}
					  ?>
						<div id="<?php echo $new_cat; ?>" class="tab-pane fade <?php echo $active_tab; ?>">
							<form method="POST" action="" enctype='multipart/form-data' class="save_inputs" id="category_<?php echo $cat->category_id;?>">

								<div class="flex-between">
									<h2><?php echo strtoupper($cat->category_name); ?></h2>
									<button type="submit" class="btn btn-info btn-md submit-form">Save Inputs</button>
								</div>
								<h3 class="text-warning"><i class="fa fa-warning"></i> Before you shut your browser window or move away from it, or before you close the lid on your laptop or logout - you <strong>MUST SAVE</strong> your work first.</h3>
								<input type="hidden" name="category_id" id="categ_<?php echo $cat->category_id; ?>" value="<?php echo $cat->category_id; ?>" class="category_id">
									<input type="hidden" id="lid_<?php echo $cat->category_id; ?>" class="last_id" name="last_id" value="">	
								  <div class="input_area">
									<?php $counter2 = 0; 
									foreach ($formdata as $styles) {

										if( $styles->category_id == $cat->category_id ) {
										  echo '<input type="hidden" name="form_id" value="'.$styles->form_id.'">';

										  $content  = json_decode($styles->input_arr);
										  $word_radio = 'type="radio"';
										  $word_checkbox = 'type="checkbox"';
										  $class_txt = 'div_question';
										  $file_type = 'type="file"';
										  $title = 'class="title"';
										  $phone_opt_class = "phone_opt";


											$display = "";
											$counter = 0;
											foreach ($content as $cont) { $counter++; 							   	
															$phrase  = $cont;
															$find = array(
																"[", 
																"]",
															  "<>"
															);
															$replace   = array(
																"<", 
																">",
															  "[]"
															);

															$content = str_replace($find, $replace, $phrase);

															if(strpos($cont, $word_checkbox) !== false) {
																echo "<div class='form-group'>";
																	echo   '<div class="checkbox">
																			  <label>'.$content.'</label>
																			</div>';
																echo '</div>';
															}elseif(strpos($cont, $word_radio) !== false) {
																echo "<div class='form-group'>";
																  echo   '<div class="radio">
																			  <label>'.$content.'</label>
																			</div>';
																echo '</div>';
															}elseif(strpos($cont, $class_txt) !== false) {
																echo "<div class='form-group'>";
																	echo $content;
																	echo   '<input type="hidden" name="question_'.$counter.'" value="'.strip_tags($content).'">';
																echo '</div>';
															}elseif(strpos($cont, $file_type) !== false) {
																echo "<div class='form-group'>";
													

																	echo $content;		
																	echo   '<input type="hidden" name="file_txt_'.$counter.'" value="" class="file_txt">';														echo '</div>';
															}elseif(strpos($cont, $title) !== false) {
																echo "<div class='form-group'>";
																	echo $content;
																	echo   '<input type="hidden" name="title_txt_'.$counter.'" value="'.strip_tags($content).'" class="title_txt">';
																echo '</div>';
															}elseif(strpos($cont, $phone_opt_class) !== false) {
																echo "<div class='form-group dflex_input'>";
																	echo $content;
																echo "</div>";
															
															}else{														
																echo "<div class='form-group'>";
																	echo $content;
																echo "</div>";
															}

													
												
											}
														  
										 
										}
									} ?>
									</div>
									<input type="hidden" class="per_token" value="0">
									<input type="hidden" id="percent_<?php echo $new_cat_id; ?>" class="percentage" name="percentage" value="0">
									<div class="prog" id="prog_<?php echo $new_cat_id; ?>">
										<h4 class="small font-weight-bold tr">Maximum Criteria Required: <span>0%</span></h4>
										<div class="progress">
											<div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0"></div>
										</div>
									</div>
									
								  <button type="submit" class="btn btn-info btn-md submit-form">Save Inputs</button>
							</form>      
						</div>
						<?php } ?>
					<?php endforeach; ?>
				</div>
				<div class="text-center" id="bma_div">
				</div>

			<?php endif; ?>

	  </div>

  <div class="alert alert-danger alert-dismissible fade show" role="alert" id="alert_err">
	<div id="alert_bcntr" style="display: inline-block;"></div>
  </div>
</div>

