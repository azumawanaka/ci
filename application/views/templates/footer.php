        

        </main>
        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
              <div class="copyright text-center my-auto">
                <span><a href="https://www.commtogether.com.au" target="_blank">Copyright &copy; CommTogether 2020</a></span>
              </div>
            </div>
        </footer>
        <!-- End of Footer -->
    </body>
    <!-- scripts -->
    <script src="<?=base_url('assets/js/vendor/jquery/jquery.min.js');?>"></script>
    <script src="<?=base_url('assets/js/vendor/bootstrap/js/bootstrap.min.js');?>"></script>

  	<!-- Core plugin JavaScript-->
  	<script src="<?=base_url('assets/js/vendor/jquery-easing/jquery.easing.min.js');?>"></script>
    <script src="<?=base_url('assets/js/common.js');?>"></script>
    <!-- //scripts -->

</html>