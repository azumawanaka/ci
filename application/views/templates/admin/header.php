
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>SWT - <?= $title ?></title>

        <!-- Custom fonts for this template-->
        <link href="<?=base_url('assets/css/vendor/fontawesome/css/all.min.css')?>" rel="stylesheet" type="text/css">
        <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->

        <link href="<?=base_url('assets/css/vendor/datatables/dataTables.bootstrap4.min.css')?>" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link rel="stylesheet" href="<?=base_url('assets/css/main.css')?>">
        <link rel="stylesheet" href="<?=base_url('assets/css/custom.css')?>">

        <script>
            var burl = '<?php echo base_url()?>';
        </script>

    </head>
    <body id="page-top" class="<?= strtolower($title) ?>" >
        
        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

              <!-- Sidebar - Brand -->
              <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                  <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-3">APP Submission Form</div>
              </a>

              <!-- Divider -->
              <hr class="sidebar-divider my-0">

              <!-- Nav Item - Dashboard -->
              <li class="nav-item <?php if(uri_string() == "swt-admin") { echo "active"; } ?>">
                <a class="nav-link" href="swt-admin">
                  <i class="fas fa-fw fa-tachometer-alt"></i>
                  <span>Dashboard</span></a>
              </li>

              <!-- Divider -->
              <hr class="sidebar-divider m-0">

              <li class="nav-item  <?php if(uri_string() == "swt-clients") { echo "active"; } ?>">
                <a class="nav-link" href="swt-clients">
                  <i class="fas fa-fw fa-users"></i>
                  <span>Clients</span>
                </a>
              </li>
              <?php if( $_SESSION['access_mode'] == 1 ) : ?>
              <!-- Divider -->
              <hr class="sidebar-divider m-0">

              <!-- Nav Item - Pages Collapse Menu -->
              <li class="nav-item  <?php if(uri_string() == "swt-forms") { echo "active"; } ?>">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                  <i class="fas fa-fw fa-folder"></i>
                  <span>Forms</span>
                </a>
                <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="swt-forms">View All</a>
                    <a class="collapse-item" href="swt-category">Categories</a>
                  </div>
                </div>
              </li>
              <!-- Divider -->
              <hr class="sidebar-divider m-0">
              <li class="nav-item <?php if(uri_string() == "swt-users") { echo "active"; } ?>">
                <a class="nav-link" href="swt-users">
                  <i class="fa fa-user"></i>
                  <span>Users</span></a>
              </li>

              <?php endif; ?>
              <!-- Divider -->
              <hr class="sidebar-divider d-none d-md-block">

              <!-- Sidebar Toggler (Sidebar) -->
              <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
              </div>

            </ul>
            <!-- End of Sidebar -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

              <!-- Main Content -->
              <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                  <!-- Sidebar Toggle (Topbar) -->
                  <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                  </button>

                  <!-- Topbar Navbar -->
                  <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                    <li class="nav-item dropdown no-arrow d-sm-none">
                      <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-search fa-fw"></i>
                      </a>
                      <!-- Dropdown - Messages -->
                      <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                        <form class="form-inline mr-auto w-100 navbar-search">
                          <div class="input-group">
                            <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                              <button class="btn btn-primary" type="button">
                                <i class="fas fa-search fa-sm"></i>
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </li>

                    <!-- Nav Item - Alerts -->
                    <li class="nav-item dropdown no-arrow mx-1" style="display: none;">
                      <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-bell fa-fw"></i>
                        <!-- Counter - Alerts -->
                        <span class="badge badge-danger badge-counter">2</span>
                      </a>
                      <!-- Dropdown - Alerts -->
                      <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                        <h6 class="dropdown-header">
                          Notifications Center
                        </h6>
                        <a class="dropdown-item d-flex align-items-center" href="#">
                          <div class="mr-3">
                            <div class="icon-circle bg-success">
                              <i class="fas fa-user text-white"></i>
                            </div>
                          </div>
                          <div>
                            <div class="small text-gray-500">December 12, 2019 13:02</div>
                            <span>user_123 successfully submitted the forms.</span>
                          </div>
                        </a>
                        <a class="dropdown-item d-flex align-items-center" href="#">
                          <div class="mr-3">
                            <div class="icon-circle bg-info">
                              <i class="fas fa-info text-white"></i>
                            </div>
                          </div>
                          <div>
                            <div class="small text-gray-500">December 7, 2019 08:25</div>
                            user_123 successfully updated his/her profile.
                          </div>
                        </a>
                        <!-- <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a> -->
                      </div>
                    </li>

                    <div class="topbar-divider d-none d-sm-block" style="display: none;"></div>

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                      <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $_SESSION['adname']; ?></span>
                        <img class="img-profile rounded-circle" src="https://source.unsplash.com/user/erondu/60x60">
                      </a>
                      <!-- Dropdown - User Information -->
                      <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                       <a class="dropdown-item" href="#" data-toggle="modal" data-target="#update_acc_modal">
                          <i class="fas fa-lock fa-sm fa-fw mr-2 text-gray-400"></i>
                          Update Password
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                          <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                          Logout
                        </a>
                      </div>
                    </li>

                  </ul>

                </nav>
                <!-- End of Topbar -->

<!-- modal -->
<div id="update_acc_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="my_id" id="my_id">
        <label>Enter New Password <span class="text-danger">*</span>: </label>
        <input type="password" class="form-control" required="required" name="new_pw" minlength="4" maxlength="14" placeholder="" id="new_pw" value="">
        <p id="err_pw" class="text-warning"></p>
        <label>Re-type Password <span class="text-danger">*</span>: </label>
        <input type="password" class="form-control mb-2" required="required" placeholder="" minlength="4" maxlength="14" name="re_pw" value="" id="re_pw">
        <p id="err_re" class="text-warning"></p>
        <p id="err_in" class="text-danger"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="update_pw">Update Password</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>