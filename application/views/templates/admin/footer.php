        

		        </div>

		        <!-- Footer -->
				<footer class="sticky-footer bg-white">
					<div class="container my-auto">
					  <div class="copyright text-center my-auto">
					    <span><a href="https://www.commtogether.com.au" target="_blank">Copyright &copy; CommTogether 2020</a></span>
					  </div>
					</div>
				</footer>
				<!-- End of Footer -->

				<!-- Logout Modal-->
				<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
					  <div class="modal-content">
					    <div class="modal-header">
					      <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
					      <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					        <span aria-hidden="true">×</span>
					      </button>
					    </div>
					    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
					    <div class="modal-footer">
					      <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
					      <a class="btn btn-primary" href="swt-adminLogout">Logout</a>
					    </div>
					  </div>
					</div>
				</div>

		    </div>
		</div>

        <!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded" href="#page-top">
			<i class="fas fa-angle-up"></i>
		</a>


    </body>
    
    <!-- scripts -->
	<!-- Bootstrap core JavaScript-->
	<script src="<?=base_url('assets/js/vendor/jquery/jquery.min.js');?>"></script>
	<script src="<?=base_url('assets/js/vendor/bootstrap/js/bootstrap.bundle.min.js');?>"></script>

	<!-- Core plugin JavaScript-->
	<script src="<?=base_url('assets/js/vendor/jquery-easing/jquery.easing.min.js');?>"></script>

	<!-- Custom scripts for admin page-->
	<script src="<?=base_url('assets/js/vendor/datatables/jquery.dataTables.min.js');?>"></script>
	<script src="<?=base_url('assets/js/vendor/datatables/dataTables.bootstrap4.min.js');?>"></script>

	<script src="<?=base_url('assets/js/vendor/jquery/jquery-sortable.js');?>"></script>
    <script src="<?=base_url('assets/js/main.js');?>"></script>
    <!-- //scripts -->

    <script>
    	$(function() {
    		$("#category_tbl").DataTable({
    			// "ajax": "<?= base_url('swt-categoryList'); ?>" ,
    			// "type": "POST",
    			// "columns": [
	      //           { "data": "id" },
	      //           { "data": "cat" },
	      //           { "data": "stats" }

	      //       ],
	      //       "order": [[1, 'asc']]
    		});
		  	// $(".input_area").sortable();
			// var editor = new MediumEditor('.editable');
		});
    </script>
</html>