<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>SWT - <?= $title ?></title>

        <!-- Custom fonts for this template-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="<?php //echo base_url('assets/css/vendor/fontawesome/css/all.min.css')?>" rel="stylesheet" type="text/css">
        <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->

        <link href="<?=base_url('assets/css/vendor/datatables/dataTables.bootstrap4.min.css')?>" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link rel="stylesheet" href="<?=base_url('assets/css/main.css')?>">
        <link rel="stylesheet" href="<?=base_url('assets/css/custom.css')?>">

        <script>
            var burl = '<?php echo base_url()?>';
        </script>

    </head>

    <body class="bg-gradient-primary">