<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'home';
$route['swt-admin'] = 'dashboard';
$route['swt-users'] = 'dashboard/adminUsers';
$route['swt-saveUser'] = 'dashboard/newUser';
$route['swt-userInfo'] = 'dashboard/userInfo';
$route['swt-delUser'] = 'dashboard/deleteUser';
$route['swt-reg'] = 'dashboard/registerAdmin';
$route['swt-adminLog'] = 'dashboard/loginAdmin';
$route['swt-adminLogout'] = 'dashboard/logoutAdmin';
$route['swt-forms'] = 'dashboard/formInputs';
$route['swt-submit'] = 'dashboard/saveForm';
$route['swt-category'] = 'dashboard/formCategory';
$route['swt-categoryList'] = 'dashboard/formCategoryList';
$route['swt-saveCategory'] = 'dashboard/saveCategory';
$route['swt-categoryInfo'] = 'dashboard/getCategory';
$route['swt-delCategory'] = 'dashboard/deleteCategory';
$route['swt-clients'] = 'dashboard/clients';
$route['swt-cReg'] = 'dashboard/register_client';
$route['swt-clientInf'] = 'dashboard/client_info';
$route['swt-delClient'] = 'dashboard/deleteClient';
$route['swt-viewData'] = 'dashboard/viewClientData';
$route['swt-forgot'] = 'dashboard/forgotpassword';
$route['swt-respass'] = 'dashboard/resetPassword';
$route['swt-conf'] = 'dashboard/confirmReset';

// client
$route['swt-uSign'] = 'home/signin';
$route['logout'] = 'home/logout';
$route['save_form'] = 'home/saveform';
$route['swt-notifyAdmin'] = 'home/emailNotification';
$route['inputs'] = 'home/clientInputs';

$route['forgotpassword'] = 'forgot';
$route['respass'] = 'forgot/respass';
$route['confirm-reset'] = 'forgot/confRes';
$route['swt-cpw'] = 'forgot/changePw';

$route['swt-create'] = "dashboard/create";

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
