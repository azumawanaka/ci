<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Forgot extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->database(); 
		$this->load->helper(array('form', 'url'));
		$this->load->library('encryption');
		$this->load->model('DashboardModel');
		$this->load->model('ClientModel');
	}

	public function index($page = 'requestpassword')
	{
		$data['title'] = ucfirst($page); 

		$this->load->view('templates/admin/headerv2', $data);
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/admin/footerv2', $data);
	}

	public function respass() {

		$pword = $this->input->post("pw");
		$email = $this->input->post("email");

		$new_password = $this->encryption->encrypt($pword);

		// check existing client
		$check = $this->DashboardModel->getClientID($email);

		if(count($check) > 0) {

				$id = $check[0]->client_id;

				$from_email = "admin@shareableapp.tk"; 
		        $to_email = $email;
		        //Load email library
		        $this->load->library('email');
		        $this->email->from($from_email, 'ShareableApp');
		        $this->email->to($to_email);
		        $this->email->subject('ShareableApp Account New Password');
		        $this->email->message("Reveal New Password: ".base_url()."confirm-reset?token_id=".$id."&token_em=".$email);
	        //Send mail
		        //Send mail
		 
		        if($this->email->send()){


				$sess_data = array(
					'last_activity'  => time()
				);

				$this->session->set_userdata($sess_data);

		         	$arr = array('msg' => 'Please check your email for your new password. Thank you', 'success' =>true);
		        }

		}else{
			$arr = array('msg' => 'Oops! The email you entered does not exist.', 'success' =>false);
		}

		echo json_encode($arr);
	}

	public function confRes($page = "newpassword") {

		$email = $_REQUEST["token_em"];
		$user_id = $_REQUEST['token_id'];

		$lastActivity = $this->session->userdata('last_activity');
		
		$check = $this->DashboardModel->getClientInfos($user_id);

		$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
		
		$new_pw = substr(str_shuffle($permitted_chars), 0, 8);


		$pw_data = array(
			'new_password'  => $new_pw
		);
		$this->session->set_userdata($pw_data);

		$new_password = $this->encryption->encrypt($this->session->userdata('new_password'));

		$data['title'] = ucfirst($page); 

		$threshold = $lastActivity + 300;

        $current_time = time();

		if($current_time >= $threshold){
			unset(
		        $_SESSION['new_password']
			);
			$data['new_pass'] = "Session expired! Please try again later.";

		}else{
			
			if(count($check) > 0) {
				if($check[0]->user_email == $email) {
					$qr = $this->ClientModel->updatePassword($new_password, $email);
					$data['new_pass'] = $this->encryption->decrypt($new_password);
				}else{
					$data['new_pass'] = "An error occured! Please try again later.";
				}

			}else{

				$data['new_pass'] = "An error occured! Please try again later.";

			}

		}


		$this->load->view('templates/admin/headerv2', $data);
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/admin/footerv2', $data);

	}

	public function changePw() {

		$email = $this->encryption->encrypt($_SESSION['email_ad']);
		$pword = $this->input->post("pw");
		$new_password = $this->encryption->encrypt($pword);
		$msg = array();

		$update = $this->DashboardModel->updatePassword($new_password,$email);
		if($update) {
			
			$update_usertbl = $this->ClientModel->updateLastAccess($_SESSION['email_ad']);

			if($update_usertbl) {
				unset(
			        $_SESSION['email_ad'],
			        $_SESSION['adname']
				);
				$msg = array("success" => true, "msg" => "Your password was successfully updated! Logging you out..");
			}

		}else{
			$msg = array("success" => false, "msg" => "Something went wrong. Please try again later.");
		}

		echo json_encode($msg);
	}

}

