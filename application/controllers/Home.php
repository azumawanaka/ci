<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->database(); 
		$this->load->helper(array('form', 'url'));
		$this->load->library('encryption');
		$this->load->model('DashboardModel');
		$this->load->model('ClientModel');
	}

	public function index($page = 'login')
	{
		if(isset($_SESSION['name'])) {
			
			$page = "home";
			$data['title'] = ucfirst($page); 
			if($_SESSION['user_access'] == 0) {

				$data['formdata'] = $this->DashboardModel->getForm();
				$data['formCategory'] = $this->DashboardModel->getFormCategory();

				$cid = $_SESSION['cid'];
				$is_paid = $_SESSION['paid_premium'];

				$qInfo = $this->DashboardModel->getClientData($cid);
				// check client final_submit col
				$data["fs"] = $this->ClientModel->checkFs($cid);

				// get data
				$cInputs = $this->ClientModel->viewInputs($cid);

				$htmlinfo = ""; 
				$htmlinfo2 = "";
			    $counter = 0;
			    $bg = "";
			    $flag = 0;

				$htmlinfo .= '<ul class="nav nav-tabs">';
				   $active_tab = ""; $counter = 0; 
				   	foreach ($data['formCategory'] as $cat):

				   		if($counter == 1) {
						  $active_tab = "in active";
						}else{
						  $active_tab = "";
						}

						$new_cat = str_replace(" ","-",$cat->category_name);
						
						$dnone = "";

						if($is_paid == 0 && $new_cat == "extra-features") {}else{  $counter++;
							if($counter == 1) {
							  $active_tab = "active";
							}else{
							  $active_tab = "";
							}
						
						  	$htmlinfo .='<li class="'.$active_tab.'"><a data-toggle="tab" href="#'.$new_cat.'">'.strtoupper($cat->category_name).'</a></li>';
						} 

					endforeach;

				$htmlinfo .= '</ul>';
    			
				$htmlinfo2 .= '<div class="tab-content">';

				$active_tab2 = ""; 
				$counter2 = 0; 
				$counter3 = 0;

				foreach ($data['formCategory'] as $cats): $counter2++;
					$new_cat2 = str_replace(" ","-",$cats->category_name);
					if($counter2 == 1) {
					  $active_tab2 = "in active";
					}else{
					  $active_tab2 = "";
					}
					$htmlinfo2 .= '<div id="'.$new_cat2.'" class="tab-pane fade '.$active_tab2.'">';
					$htmlinfo2 .= '<div class="input_area">';

					foreach ($qInfo as $info) {
						if($info->category_id == $cats->category_id) {

							foreach (json_decode($info->ci_data) as $key => $value) {

						    	if($key == "category_id") {
						    		$fc_id = $value;
						    	}

								if($key == "form_id" || $key == "last_id" || $key == "percentage" || $key == "category_id") {
									
								}else{
									if (strpos($key, 'title_txt_') !== false) { $counter3++;
										$htmlinfo2 .= "<div class='form-group'>";
										$htmlinfo2 .= "<h4 class='title'>".$value."</h4>";
										$htmlinfo2 .= '</div>';
									}elseif(strpos($key, 'question_') !== false){
										$htmlinfo2 .= "<div class='form-group'>";
										$htmlinfo2 .= "<div class='div_question'>".$value."</div>";
										$htmlinfo2 .= '</div>';
									}elseif(strpos($key, 'file_txt_') !== false){
										$htmlinfo2 .= "<div class='form-group'>";
										$data_files = explode(",", $value);
										if(!empty($value)) {
											for($i = 0; $i < count($data_files); $i++) {		
												
												if(strpos($data_files[$i], ".pdf")) {
													$htmlinfo2 .= '> <a href="uploads/'.$info->client_id."/".date("d-m-Y", $info->ci_date_sent)."/".$fc_id."/".str_replace(' ', '_', $data_files[$i]).'" download title="download file">'.$data_files[$i].'</a>';
												}else{

													$htmlinfo2 .= '> <a href="uploads/'.$info->client_id."/".date("d-m-Y", $info->ci_date_sent)."/".$fc_id."/".str_replace(' ', '_', $data_files[$i]).'" download title="download file"><img height="50" style="margin-left: 10px;" title="'.$data_files[$i].'" src="'.base_url()."uploads/".$info->client_id."/".date("d-m-Y", $info->ci_date_sent)."/".$fc_id."/".str_replace(' ', '_', $data_files[$i]).'"></a>';
												}								
											}
										}else{
											$htmlinfo2 .= "<span class='text-danger'>> No File/s Uploaded</span>";
										}
										$htmlinfo2 .= '</div>';
									}else{
										$htmlinfo2 .= "<div class='form-group'>";
										
										if(!empty($value)) {
											if(strpos($value, 'Font Family') !== false) {
												$htmlinfo2 .= "<span style='".str_replace(' ', '-',strtolower($value))."'>> ".$value."</span>";
											}else{
												$htmlinfo2 .= "<span class='text-info'>> ".$value."</span>";
											}
										}else{
											$htmlinfo2 .= "<span class='text-danger'>> No Answer</span>";
										}
									
										$htmlinfo2 .= '</div>';
									}
								}
							} 


							if($info->ci_percentage > 0 && $info->ci_percentage <= 20) {
								$bg = "#e74a3b";
							}else if($info->ci_percentage > 20 && $info->ci_percentage <= 40) {
								$bg = "#f6c23e";
							}else if($info->ci_percentage > 40 && $info->ci_percentage <= 60) {
								$bg = "#4e73df";
							}else if($info->ci_percentage > 60 && $info->ci_percentage <= 80) {
								$bg = "#36b9cc";
							}else{
								$bg = "#1cc88a";
							}
		        			
		    				$htmlinfo2 .= '<div class="prog">
			                                <h4 class="small font-weight-bold tr">Maximum Criteria Required: <span>'.$info->ci_percentage.'%</span></h4>
			                                <div class="progress">
			                                    <div class="progress-bar" role="progressbar" style="background-color: '.$bg.';width: '.$info->ci_percentage.'%" aria-valuenow="'.$info->ci_percentage.'" aria-valuemin="0" aria-valuemax="'.$info->ci_percentage.'"></div>
			                                </div>
			                            </div>';

		        			$htmlinfo2 .= '<small><i class="fa fa-calendar"></i> '.date("d M, Y H:i", $info->ci_date_sent).'</small>';


						}

					}

					$htmlinfo2 .= "</div></div>";

				endforeach;

				$htmlinfo2 .= "</div>";

				$data['htm'] = $htmlinfo.$htmlinfo2;

				$this->load->view('templates/header2', $data);
				$this->load->view('pages/'.$page, $data);
				$this->load->view('templates/footer', $data);
			}else{

				unset(
        			$_SESSION['email'],
			        $_SESSION['name']
				);

				redirect(base_url("swt-home"));
			}

		}else{
			$data['title'] = ucfirst($page); 

			$this->load->view('templates/header', $data);
			$this->load->view('pages/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
	}

	public function signin() {

		$email = $this->input->post("email");
		$pword = $this->input->post("pword");

		$flag = 0;

		// check if user exist
		$check_client = $this->ClientModel->checkClient($email);

		if(count($check_client) > 0) {
			if($pword == $this->encryption->decrypt($check_client[0]->password)) {

				$newdata = array(
					'user_access' => 0,
					'cid'  => $check_client[0]->client_id,
			        'name'  => $check_client[0]->name,
			        'email'     => $check_client[0]->user_email,
			        'paid_premium' => $check_client[0]->paid_premium
				);

				$this->session->set_userdata($newdata);

				$flag = 1;
			}else{

				$flag = 0;
			}
		}else{
			$flag = 0;
		}

		echo $flag;
	}

	public function logout() {
		@$email = $_SESSION['email'];
		$update_usertbl = $this->ClientModel->updateLastAccess($email);
		$flag = false;

		if($update_usertbl) {
			unset(
		        $_SESSION['email'],
		        $_SESSION['name']
			);

			// redirect(base_url());

			$flag = true;
		}else{
			$flag = false;
		}

		echo $flag;

	}

	public function saveform() {

		$flag = 0;
		$msg = array();

		$lastid = $_REQUEST["last_id"];

		$cat_id = $_REQUEST["category_id"];
		
		$user_id = $_SESSION['cid'];

		$dateFile = date('d-m-Y');


		$data_info = array(
			"form_id" => $this->input->post('form_id'),
			"client_id" => $user_id,
			"category_id" => $cat_id,
			"ci_data" => json_encode($_REQUEST),
			"ci_percentage" => $this->input->post("percentage"),
			"ci_date_sent" => strtotime("now")
		);

    	if( !empty($lastid) ) {
    		$qs = $this->ClientModel->updateInputs($data_info, $lastid);
    		if($qs > 0) {
    			$msg = array('success' => true, 'messages' => "<strong>Success!</strong> You have successfully update data.", 'last_id' => $qs);
    		}else{
    			$msg = array('success' => false, 'messages' => "<strong>Error!</strong> An error was found please try again later.", 'last_id' => $qs);
    		}
    	}else{
			$qs = $this->ClientModel->saveInputs($data_info);
    		if($qs > 0) {
    			$msg = array('success' => true, 'messages' => "<strong>Success!</strong> You have successfully save data.", 'last_id' => $qs);
    		}else{
    			$msg = array('success' => false, 'messages' => "<strong>Error!</strong> An error was found please try again later.", 'last_id' => "");
    		}
    	}


		$data = array();
		if(isset($_FILES['filename']['name'])) {
      		$count = count($_FILES['filename']['name']);
      	}else{
      		$count = 0;
      	}
      	if($count > 0) {
	      	for($i=0; $i<$count; $i++){

		        if(!empty($_FILES['filename']['name'][$i])){
		        	
					$_FILES['file']['name'] = $_FILES['filename']['name'][$i];

					$_FILES['file']['type'] = $_FILES['filename']['type'][$i];

					$_FILES['file']['tmp_name'] = $_FILES['filename']['tmp_name'][$i];

					$_FILES['file']['error'] = $_FILES['filename']['error'][$i];

					$_FILES['file']['size'] = $_FILES['filename']['size'][$i];

			        $config['upload_path'] = 'uploads/'.$user_id."/".$dateFile."/".$_REQUEST['category_id'];

					if (!file_exists($config['upload_path'])) {
						mkdir($config['upload_path'], 0777, true);
					}
		        	// $config['overwrite'] = TRUE;

		         	$config['allowed_types'] = '*';

		          	$config['max_size'] = '9999999';

		          	$config['file_name'] = $_FILES['filename']['name'][$i];

		   
		          	$this->load->library('upload',$config); 

		          	$this->upload->initialize($config);

		          	if(file_exists($config["upload_path"]."/".$_FILES['filename']['name'][$i])) {
		          		unlink($config['upload_path']."/".$_FILES['filename']['name'][$i]);
		          	}

					if($this->upload->do_upload('file')){

						$uploadData = $this->upload->data();

						$filename = $uploadData['file_name'];

						$data['totalFiles'][] = $filename;


						$flag = 1;
					}else{
						$flag = 0;
					}

		        }else{
		        	$flag = 1;
		        }

		    }
		}else{
			// no file
			$flag = 1;
		}
		
		if($flag == 1) {
			echo json_encode($msg);
		}

	}

	public function clientInputs() {
		// fetch data by client id
		$cid = $this->input->post("id");

		$qInfo = $this->DashboardModel->getClientData($cid);

		$fCategory = $this->DashboardModel->getFormCategory();

		$data_arr = array();
		$data_arr2 = array();
		$data_arr3 = array();


		foreach ($fCategory as $cats) {
			$cat_id = $cats->category_id;
			$query_form_id = $this->ClientModel->getFormID($cat_id);

			$data_arr3[] = array(
				"cid"	=> $cats->category_id,
				"formID" => $query_form_id[0]->form_id
			);
		}

		foreach ($qInfo as $ids) {
			$data_arr2[] = array(
				"ci_id" => $ids->ci_id,
				"cat"	=> $ids->category_id,
				"per" => $ids->ci_percentage
			);
		}
		for($i = 0; $i < count($qInfo); $i++) {
			foreach (json_decode($qInfo[$i]->ci_data) as $key => $value) {
				$data_arr[] = array(
					"data_key" => $key,
					"data_val" => $value
				);
			}
		}

		echo json_encode(array("inputs" => $data_arr, "ids" => $data_arr2, "cats" => $data_arr3));
	}

	public function emailNotification() {
		$user_id = $this->input->post("sess_id");
		$date = date("d M, Y", strtotime("now"));

		$flag = 0;
		// check client 
		$q_client = $this->DashboardModel->getClientInfos($user_id);

		// update client tbl
		$update_client = $this->ClientModel->updateClient($user_id);

		if($update_client) {


	        $content = "Hi
".$q_client[0]->name." - ".$q_client[0]->user_email." has now completed the form to build the app

Template: Hotel

You can access his data here ".base_url('swt-admin/clients')."

Thanks";


			$from_email = $q_client[0]->user_email;
			$to_email = array('nelson.paloma@gmail.com'); //admin
			$bcc = array('nelson.paloma@outlook.com');
			$this->load->library('email');
			$this->email->from($from_email, 'Paradises App Info');
			$this->email->to($to_email);
			$this->email->bcc($bcc); //bcc
			$this->email->subject('Paradises App Info');
			$this->email->message($content);
			

	        if($this->email->send()) {
	        	$flag = 1;
	        }else{
	        	$flag = 0;
	        }

		}else{
			$flag = 0;
		}


		echo $flag;
	}

}
