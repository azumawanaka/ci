<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->database(); 
		$this->load->helper(array('form', 'url'));
		$this->load->library('encryption');
		$this->load->model('DashboardModel');
		$this->load->library('zip');
	}

	public function index($page = 'dashboard')
	{
		$data['title'] = ucfirst($page); 

		if(isset($_SESSION['adname'])) {

			if($_SESSION['access_mode'] > 0) {

				$data["total_client"] = count($this->DashboardModel->getClients());
				$qa = $this->DashboardModel->getQA();
				$qa_count = 0;
				foreach ($qa as $qa_arr) {
					foreach (json_decode($qa_arr->input_arr) as $key_arr) {
						$qa_count++;
					}	
				}
				$data["total_qa"] = $qa_count;
				
				$c_data = $this->DashboardModel->getCdata();
				$cdata_count = 0;
				foreach ($c_data as $cdata_arr) {
					foreach (json_decode($cdata_arr->ci_data) as $key_cdata) {
						$cdata_count++;
					}	
				}
				$data["total_cd"] = $cdata_count;

				$this->load->view('templates/admin/header', $data);
				$this->load->view('pages/admin/'.$page, $data);
				$this->load->view('templates/admin/footer', $data);

			}else{

				unset(
        			$_SESSION['email'],
			        $_SESSION['adname']
				);

				redirect(base_url("swt-home"));
			}

		}else{
			$page = 'login';

			$check_admin = $this->DashboardModel->getAdminCount();
			$this->load->view('templates/admin/headerv2', $data);

			if($check_admin > 0) {
				$this->load->view('pages/admin/'.$page, $data);
			}else{
				$page = 'signup';
				$data['title'] = ucfirst($page); 
				$this->load->view('pages/admin/'.$page, $data);
			}

			$this->load->view('templates/admin/footerv2', $data);

		}

	}

	public function adminUsers($page = 'admin') {
		if(isset($_SESSION['adname'])) {

			$data['title'] = ucfirst($page); 

			$data['users'] = $this->DashboardModel->getUserAdmin();

			$this->load->view('templates/admin/header', $data);
			$this->load->view('pages/admin/'.$page, $data);
			$this->load->view('templates/admin/footer', $data);

		}else{
			redirect(base_url("swt-admin"));
		}
	}

	public function newUser() {

		$token = $this->input->post("utoken");
		$name = $this->input->post("un");
		$email = $this->input->post("em");
		$pword = $this->encryption->encrypt($this->input->post("pw"));

		// check if user already exist

		$flag = 0;

		

		$data = array(
			"name" => $name,
			"email" => $email,
			"password"	=> $pword,
			"access_mode" => 3,
			"date_registered" => strtotime("now")
		);


		if($token!="") {
			$upU = $this->DashboardModel->updateAdminInfo($data, $token);
			if($upU) {
				$flag = 3;
			}else{
				$flag = 0;
			}
		}else{
			$checkUser = $this->DashboardModel->checkAdmin($email);
			if(count($checkUser) > 0) {
				$flag = 2; 
			}else{

				$save_data = $this->DashboardModel->saveAdminInfo($data);

				if($save_data) {
					$flag = 1;
				}else{
					$flag = 0;
				}

			}

		}

		echo $flag;
	}

	public function deleteUser() {
		$uid = $this->input->post("uid");
		$qd = $this->DashboardModel->userDelete($uid);

		if($qd) {
			$flag = 1;
		}else{
			$flag = 0;
		}
		
		echo $flag;
	}

	public function userInfo() {
		$email = $this->input->post("email");

		$get_data = $this->DashboardModel->checkAdmin($email);

		$new_arr = array();

		foreach ($get_data as $u) {
			$d_arr = array(
				"user_id" => $u->user_id,
				"name"	=> $u->name,
				"email"	=> $u->email,
				"password"	=> $this->encryption->decrypt($u->password)
			);

			array_push($new_arr, $d_arr);
		}

		echo json_encode($new_arr);
	}

	public function registerAdmin() {
		$name = $_POST["name"];
		$email = $_POST["email"];
		$pword = $_POST["password"];
		$pword2 = $this->encryption->encrypt($_POST["passwordv2"]);

		$flag = 0;

		if($pword != $_POST["passwordv2"]) {
			
			$flag = 2;

		}else{


			$data = array(
				"name" => $name,
				"email" => $email,
				"password"	=> $pword2,
				"date_registered" => strtotime("now")
			);

			$save_data = $this->DashboardModel->saveAdminInfo($data);

			if($save_data) {
				$flag = 1;
			}else{
				$flag = 0;
			}

		}

		echo $flag;
	}

	public function loginAdmin() {

		$email = $_POST["email"];
		$pword = $_POST["password"];

		$flag = 0;

		// check if user exist
		$check_admin = $this->DashboardModel->checkAdmin($email);

		if(count($check_admin) > 0) {
			if($pword == $this->encryption->decrypt($check_admin[0]->password)) {

				$newdata = array(
						'access_mode'	=> $check_admin[0]->access_mode,
				        'adname'  => $check_admin[0]->name,
				        'email_ad'     => $check_admin[0]->email
				);

				$this->session->set_userdata($newdata);

				$flag = 1;
			}else{

				$flag = 0;
			}
		}else{
			$flag = 0;
		}

		echo $flag;
	}

	public function logoutAdmin() {
		

		$update_usertbl = $this->DashboardModel->updateLastAccess($_SESSION['email_ad']);

		unset(
	        $_SESSION['email_ad'],
	        $_SESSION['adname']
		);

		redirect(base_url("swt-admin"));
	}

	public function formCategory($page = 'category') {

		if(isset($_SESSION['adname'])) {

			$data['title'] = ucfirst($page); 

			$data['formCategory'] = $this->DashboardModel->getFormCategory();

			$this->load->view('templates/admin/header', $data);
			$this->load->view('pages/admin/'.$page, $data);
			$this->load->view('templates/admin/footer', $data);
		}else{
			redirect(base_url("swt-admin"));
		}
	}

	public function formInputs($page = 'forms') {
	
		if(isset($_SESSION['adname'])) {

			$data['title'] = ucfirst($page); 

			$data['formdata'] = $this->DashboardModel->getForm();
			$data['formCategory'] = $this->DashboardModel->getFormCategory();

			$this->load->view('templates/admin/header', $data);
			$this->load->view('pages/admin/'.$page, $data);
			$this->load->view('templates/admin/footer', $data);

		}else{
			redirect(base_url("swt-admin"));
		}
	}

	public function saveForm() {

		$form_data = strip_tags(json_encode($this->input->post("data_info")), '<br>');
		$cat = $this->input->post("cat");
		$formID = $this->input->post("formid");
		$flag = 0;

		$data = array(
			"category_id" => $cat,
			"input_arr" => $form_data,
			"date_saved" => strtotime("now")
		);


		if (!empty($formID)) {
			if($this->DashboardModel->updateForm($data, $formID)){
				$flag = 1;
			}else{
				$flag = 0;
			}
		}else{
			if($this->DashboardModel->saveFormNow($data)) {
				$flag = 1;
			}else{
				$flag = 0;
			}
		}

		echo $flag;
	}

	public function saveCategory() {

		$cat = $this->input->post("cat");
		$catid = $this->input->post("catid");

		$flag = 0;

		$data = array(
			"category_name" => $cat
		);

		if(!empty($catid)) {
			if($this->DashboardModel->updateCategory($data, $catid)){
				$flag = 2;
			}else{
				$flag = 0;
			}
		}else{
			if($this->DashboardModel->saveCategory($data)){
				$flag = 1;
			}else{
				$flag = 0;
			}
		}

		echo $flag;
	}

	public function getCategory() {

		$cat_id = $this->input->post("cat_id");
		$formdata = $this->DashboardModel->getCategoryInfo($cat_id);

		echo json_encode($formdata);
	}

	public function deleteCategory() {

		$c_id = $this->input->post("c_id");
		$flag = 0;

		if($this->DashboardModel->delCategory($c_id)) {
			$flag = 1;
		}else{
			$flag = 0;
		}

		echo $flag;
	}

	public function clients($page = "clients") {
		if(isset($_SESSION['adname'])) {
			if($_SESSION['access_mode'] > 0) {
				$data['title'] = ucfirst($page); 

				$data['clients'] = $this->DashboardModel->getClients();

				$this->load->view('templates/admin/header', $data);
				$this->load->view('pages/admin/'.$page, $data);
				$this->load->view('templates/admin/footer', $data);
			}else{
				redirect(base_url("swt-home"));
			}
		}else{
			redirect(base_url("swt-admin"));
		}
	}

	public function register_client() {

		$ctoken = $this->input->post("ctoken");
		$name = $this->input->post("c_name");
		$email = $this->input->post("c_email");
		$pword = $this->input->post("c_pword");
		$cpremium = $this->input->post("c_premium");

		if(isset($cpremium) && !empty($cpremium)) {
			$cpremium = $this->input->post("c_premium");
		}else{
			$cpremium = 0;
		}

		$flag = 0;

		$data = array(
			"user_email" => $email,
			"password"	=> $this->encryption->encrypt($pword),
			"name"	=> $name,
			"paid_premium" => $cpremium,
			"date_registered" => strtotime("now")
		);

		$arr = array();
		if(!empty($ctoken)) {
			// update client
			$update_client = $this->DashboardModel->updateClient($data, $ctoken);
			if($update_client > 0) {
				$arr = array('msg' => 'Client successfully updated!', 'success' =>true);
			}else{
				$arr = array('msg' => 'Something went wrong try again later', 'success' =>false);
			}
		}else{
			// check first if email already exist
			$check_email = $this->DashboardModel->checkClient($email);
			if($check_email > 0) {
				$arr = array('msg' => 'Email already exist', 'success' =>false);
			}else{

				$save_data = $this->DashboardModel->saveClient($data);

				if($save_data) {
					$flag = 1;

					$from_email = "no-reply@paradises.commtogether.com.au"; //this will be use as administrator of the website email address
			        $to_email = $email;

			        $msg = 'Hi '.$name.', 
			        
Please click on the following link to register all the information we need to complete the build of your app.
'.base_url().'

Login:  '.$email.'
Password:  '.$pword.'

If you have any questions or require assistance, please email production@paradises.commtogether.com.au';

			        //Load email library
			        $this->load->library('email');
			        $this->email->from($from_email, 'Paradises App');
			        $this->email->to($to_email);
			        $this->email->subject('Paradises app information register');
			        $this->email->message($msg);
			        //Send mail

			        $arr = array('msg' => 'Something went wrong try again later', 'success' =>false);
			 
			        if($this->email->send()){
			         	$arr = array('msg' => 'Client successfully saved', 'success' =>true);
			        }

				}else{
					$arr = array('msg' => 'Something went wrong, please try again later', 'success' =>false);
				}
			}
		}

		echo json_encode($arr);
	}

	public function client_info() {
		$cid = $this->input->post("c_id");
		$get_data = $this->DashboardModel->showClientInfo($cid);

		$new_arr = array();

		foreach ($get_data as $client) {
			$d_arr = array(
				"name"	=> $client->name,
				"email"	=> $client->user_email,
				"password"	=> $this->encryption->decrypt($client->password),
				"paid_premium" => $client->paid_premium
			);

			array_push($new_arr, $d_arr);
		}
		echo json_encode($new_arr);
	}

	public function deleteClient() {
		$cid = $this->input->post("c_id");
		$del_q = $this->DashboardModel->delClient($cid);
		$flag = 0;
		if($del_q) {
			$flag = 1;
		}else{
			$flag = 0;
		}

		echo $flag;
	}


    public function viewClientData() {
        $cid = $this->input->post("cid");

        $qInfo = $this->DashboardModel->getClientData($cid);

        $htmlinfo = ""; 
        $counter = 0;
        $bg = "";
        $flag = 0;

		$htmlinfo .= '<ul>';
        foreach ($qInfo as $info) {
        	if(empty($info->client_id)) {
        		$flag = 0;
        	}else{
        		$flag = 1;
        	}
			
			foreach (json_decode($info->ci_data) as $key => $value) {

        	if($key == "category_id") {
        		$fc_id = $value;
        	}

			$htmlinfo .='<li class="mb-2">';
				if($key == "form_id" || $key == "last_id" || $key == "percentage") {
					
				}elseif($key == "category_id"){
					
        			$formCategory = $this->DashboardModel->getFormCategoryByID($value);

        			foreach($formCategory as $fc) {
        				if($fc->category_id == $fc_id) {
        					$htmlinfo .= "<h1>".strtoupper($fc->category_name)."</h1>";
        					if($info->ci_percentage > 0 && $info->ci_percentage <= 20) {
								$bg = "#e74a3b";
							}else if($info->ci_percentage > 20 && $info->ci_percentage <= 40) {
								$bg = "#f6c23e";
							}else if($info->ci_percentage > 40 && $info->ci_percentage <= 60) {
								$bg = "#4e73df";
							}else if($info->ci_percentage > 60 && $info->ci_percentage <= 80) {
								$bg = "#36b9cc";
							}else{
								$bg = "#1cc88a";
							}
		        			
	        				$htmlinfo .= '<div class="prog">
			                                <h4 class="small font-weight-bold tr">Progress <span>'.$info->ci_percentage.'%</span></h4>
			                                <div class="progress">
			                                    <div class="progress-bar" role="progressbar" style="background-color: '.$bg.';width: '.$info->ci_percentage.'%" aria-valuenow="'.$info->ci_percentage.'" aria-valuemin="0" aria-valuemax="'.$info->ci_percentage.'"></div>
			                                </div>
			                            </div>';

		        			$htmlinfo .= '<small><i class="fa fa-calendar"></i> '.date("d M, Y", $info->ci_date_sent).'</small>';
        				}
        			}

				}else{

					if (strpos($key, 'title_txt_') !== false) { $counter++;
					    $htmlinfo .= "<strong>".$value."</strong>";
					}elseif(strpos($key, 'question_') !== false){
						$htmlinfo .= "<h4 class='text-danger q_a'>Question: </h4> ".$value;
					}elseif(strpos($key, 'file_txt_') !== false){
						$htmlinfo .= "Files: ";
						$data_files = explode(",", $value);
						for($i = 0; $i < count($data_files); $i++) {		
							
							if(strpos($data_files[$i], ".pdf")) {
								$htmlinfo .= '<a href="uploads/'.$info->client_id."/".date("d-m-Y", $info->ci_date_sent)."/".$fc_id."/".str_replace(' ', '_', $data_files[$i]).'" download title="download file">'.$data_files[$i].'</a>';
							}else{

								$htmlinfo .= '<img height="25" style="margin-left: 10px;" title="'.$data_files[$i].'" src="'.base_url()."uploads/".$info->client_id."/".date("d-m-Y", $info->ci_date_sent)."/".$fc_id."/".str_replace(' ', '_', $data_files[$i]).'">';
							}								
						}
					}else{
						$htmlinfo .= "<h4 class='text-info q_a'>Answer: </h4> ".$value;
					}
				}
				$htmlinfo .= '</li>';
			}        	
        }

	    $htmlinfo .= '</ul>';
	    if($flag == 1) {
        	echo "<div class='client_data_info'>".$htmlinfo."</div>";
    	}else{
    		echo 0;
    	}
    }

    public function forgotpassword($page = 'forgotpassword') {
    	$data['title'] = ucfirst($page); 

		$this->load->view('templates/admin/headerv2', $data);
		$this->load->view('pages/admin/'.$page, $data);
		$this->load->view('templates/admin/footerv2', $data);
    }

    public function resetPassword() {

		$pword = $this->input->post("pw");
		$email = $this->input->post("email");

		$new_password = $this->encryption->encrypt($pword);

		// check existing admin user
		$check = $this->DashboardModel->checkAdmin($email);

		if(count($check) > 0) {

			$id = $check[0]->user_id;

			$from_email = "admin@shareableapp.tk"; 
	        $to_email = $email;
	        //Load email library
	        $this->load->library('email');
	        $this->email->from($from_email, 'ShareableApp');
	        $this->email->to($to_email);
	        $this->email->subject('ShareableApp Account New Password');
	        $this->email->message("Reveal New Password: ".base_url()."swt-conf?token_id=".$id."&token_em=".$email);
		        //Send mail
		 
	        if($this->email->send()){


				$sess_data = array(
					'last_activity'  => time()
				);

				$this->session->set_userdata($sess_data);

	         	$arr = array('msg' => 'Please check your email for your new password. Thank you', 'success' =>true);
	        }

		}else{
			$arr = array('msg' => 'Oops! The email you entered does not exist.', 'success' =>false);
		}

		echo json_encode($arr);

	}

	public function confirmReset($page = "newpassword") {

		$email = $_REQUEST["token_em"];
		$user_id = $_REQUEST['token_id'];

		$lastActivity = $this->session->userdata('last_activity');
		
		$check = $this->DashboardModel->checkAdminID($user_id);

		$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
		
		$new_pw = substr(str_shuffle($permitted_chars), 0, 8);

		$pw_data = array(
			'new_password'  => $new_pw
		);
		$this->session->set_userdata($pw_data);

		$new_password = $this->encryption->encrypt($this->session->userdata('new_password'));

		$data['title'] = ucfirst($page); 

		$threshold = $lastActivity + 300;

        $current_time = time();

		if($current_time >= $threshold){
			unset(
		        $_SESSION['new_password']
			);
			$data['new_pass'] = "Session expired! Please try again later.";

		}else{
			
			if(count($check) > 0) {
				if($check[0]->email == $email) {
					$qr = $this->DashboardModel->updatePassword($new_password,$email);

					$data['new_pass'] = $this->encryption->decrypt($new_password);
				}else{
					$data['new_pass'] = "An error occured! Please try again later.";
				}

			}else{

				$data['new_pass'] = "Error! Account not found!";

			}

		}

		$this->load->view('templates/admin/headerv2', $data);
		$this->load->view('pages/admin/'.$page, $data);
		$this->load->view('templates/admin/footerv2', $data);

	}

	// create and download method
    public function create() {
        $filename = 'example.txt';
        $filedata = 'This is an example file. Codeigniter zip class is 
                     used to zip, save and download the file';
        $this->zip->add_data($filename, $filedata); 

        $this->zip->read_dir("uploads/"); 

        $this->zip->archive('zippedfiles/example_backup.zip');  
        $this->zip->download('example_backup.zip');
    } 

}
